defmodule Server.MixProject do
  use Mix.Project

  def project do
    [
      app: :server,
      version: "0.1.0",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Application.get_env(:server, :env)),
      compilers: Mix.compilers(),
      start_permanent: Application.get_env(:server, :env) == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: releases()
    ]
  end

  defp releases do
    [
      server: [
        include_erts: true,
        path: "live_track_runtime",
        include_executables_for: [:unix],
        applications: [
          logger: :permanent,
          runtime_tools: :permanent,
          server: :permanent,
          websockex: :permanent
        ]
      ]
    ]
  end

  def application do
    [
      mod: {Server, []},
      extra_applications: [:logger, :runtime_tools, :websockex]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:mock, "~> 0.3.7", only: :test},
      {:websockex, "~> 0.4.3"},

      {:libcluster, "~> 3.2"},
      # {:munin, git: "http://172.25.78.108/scandinavians/munin.git", tag: "0.1.20"},

      {:poison, "~> 4.0"},
      {:jason, "~> 1.2"},
      {:morphix, "~> 0.8.0"},
      {:gettext, "~> 0.20"},

      {:cowboy, "~> 2.8"},
      {:plug, "~> 1.0"},
      {:plug_cowboy, "~> 2.5"},
      {:geo, "~> 3.4.2"},
      {:geocalc, "~> 0.8"},
    ]
  end

  defp aliases do
    [
      test: "test --no-start"
    ]
  end
end
