# base image elixer to start with
FROM centos:centos7.4.1708

RUN yum makecache && yum -y install curl git && yum -y install zip
RUN yum -y install gcc gcc-c++ glibc-devel make ncurses-devel openssl-devel autoconf java-1.8.0-openjdk-devel git wget wxBase.x86_64 unzip
RUN PATH="$HOME/.asdf/bin:$HOME/.asdf/shims:$PATH" && \
    echo "PATH=$HOME/.asdf/bin:$HOME/.asdf/shims:$PATH" >> ~/.bashrc && \
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.2
RUN source ~/.bashrc && asdf plugin add erlang && asdf install erlang 25.0 && asdf global erlang 25.0 && \
    asdf plugin add elixir && asdf install elixir 1.14.0-rc.0-otp-25 && asdf global elixir 1.14.0-rc.0-otp-25