class Socket {
  constructor() {
    this.uri = "ws://" + location.host + "/ws/chat";
    this.websocket;
    this.output;
    this.messageBoxSendButton;
    this.messageBoxInput;
  }

  findRoom = () => {
    return Array.from(document.querySelectorAll('input[name="channel"]')).filter(el => el.checked)[0].value
  }
  linkRooms = () => {
    Array.from(document.querySelectorAll('input[name="channel"]')).map(el => el.addEventListener('click', (_evt) => this.joinRoom()))
    return "ok"
  }
  initWS = () => {
    this.websocket = new WebSocket(
      this.uri + "?access_token=" + this.getParameterByName("access_token")
    );
    this.websocket.onopen    = (evt) => this.onOpen(evt);
    this.websocket.onclose   = (evt) => this.onClose(evt);
    this.websocket.onmessage = (evt) => this.onMessage(evt);
    this.websocket.onerror   = (evt) => this.onError(evt);
  }
  init = () => {
    this.initWS();
    this.output = document.getElementById("output");
    this.messageBoxInput = document.getElementById("message_box_input");
    this.messageBoxSendButton = document.getElementById("message_box_send_button");
    this.messageBoxSendButton.addEventListener('click', () => this.sendMessage());
    this.linkRooms();
  }
  createRoom = () => {
    this.doSend(JSON.stringify({
      command: "create",
      room: this.findRoom(),
    }))
  }
  joinRoom = () => {
    document.querySelector("#output").replaceChildren([]);
    this.doSend(JSON.stringify({
      command: "join",
      room: this.findRoom(),
    }));
  }
  sendMessage = () => {
    this.doSend(JSON.stringify({
      room: this.findRoom(),
      message: this.messageBoxInput.value,
    }));
  }

  onOpen = (evt) =>  {
    // this.writeToScreen("CONNECTED");
    // this.doSend(JSON.stringify({ command: "join" }));
    let log_window = document.querySelector("#com_window");
    log_window.classList.remove("border-purple-600");
    log_window.classList.remove("shadow-purple-800/50");
    log_window.classList.add("border-green-600");
    log_window.classList.add("shadow-green-800/50");
    this.joinRoom();
  }
  onClose = (evt) =>  {
    this.writeToScreen("DISCONNECTED");
    let log_window = document.querySelector("#com_window")
    log_window.classList.remove("border-green-600")
    log_window.classList.remove("shadow-green-800/50")
    log_window.classList.add("border-red-600")
    log_window.classList.add("shadow-red-800/50")
  }
  onMessage = (evt) =>  {
    let response = JSON.parse(evt.data);
    if (this.isError(response)) {
      this.writeToScreen(
        '<span style="color: red;">ERROR: ' + response.error + "</span>"
      );
    } else if (this.isSuccess(response)) {
      this.writeToScreen(
        '<span style="color: green;">SUCCESS: ' + response.success + "</span>"
      );
    } else if (this.isSystemMessage(response)) {
      this.writeToScreen(
        '<span style="color: gray;">' +
          response.room +
          ": " +
          response.message +
          "</span>"
      );
    } else {
      var d = new Date();
      this.writeToScreen(
        '<span style="color: blue;">[' +
          (d.getUTCHours() + ':' + d.getUTCMinutes() + ':' + d.getUTCSeconds()) +
          "] (" +
          response.from +
          ") " +
          response.message +
          "</span>"
      );
    }
  }
  onError = (evt) => this.writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
  writeToScreen = (message) =>  {
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = message;
    this.output.appendChild(pre);
  }
  doSend = (message) =>  {
    // this.writeToScreen("SENT: " + message);
    this.websocket.send(message);
  }
  isError = (response) =>  response.error != undefined;
  isSuccess = (response) =>  response.success != undefined;
  isSystemMessage = (response) => response.from == undefined
  getParameterByName = (name) =>  {
    var match = RegExp("[?&]" + name + "=([^&]*)").exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
  }  
}
let sock = new Socket()
window.addEventListener("load", sock.init, false);
