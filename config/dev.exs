import Config

config :logger, :console, format: "[$level] $message\n"
config :server, :env, :dev
