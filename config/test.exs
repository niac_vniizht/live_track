import Config

config :server,
  server_name: "test_live_track",
  default_page_size: 100,
  max_page_size: 500,
  site_name: "Test site",
  max_machines: 100,
  port: 9091,
  wialon_password: ["valid_password", "NA"]

config :server,
  wialon_port: 5040,
  scout_port: 20653,
  niac_port: 7001,
  mvps_port: 7002

config :logger, level: :warn
config :server, :env, :test
