# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

addr =
    :inet.getif()
    |> elem(1)
    |> Enum.filter(& elem(&1, 2) == {255, 255, 255, 0})
    |> Enum.map(& elem(&1, 0))
    |> List.first
    |> Kernel.||({0, 0, 0, 0})

config :server,
  server_name: "live_track",
  default_page_size: 100,
  max_page_size: 500,
  site_name: "Test site",
  max_machines: 100,
  port: 5039,
  ws_addr: addr,
  ws_port: 9888,
  ets_storage: :test_store,
  # string || list
  wialon_password: ["valid_password", "NA"]

config :server,
  wialon_port: 5039,
  scout_port: 20654,
  niac_port: 9875,
  comm_port: 9875,
  mvps_port: 9878

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

import_config "#{config_env()}.exs"
