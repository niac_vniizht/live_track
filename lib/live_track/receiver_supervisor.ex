defmodule LiveTrack.ReceiverSupervisor do
  use DynamicSupervisor
  @supervisor_name :rec_supervisor
  alias LiveTrack.RecRegistry

  def create(name, port) do
    {:ok, _pid} = find_or_create(name, port)
  end

  def start_link(_opts) do
    DynamicSupervisor.start_link(__MODULE__, [], name: @supervisor_name)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one, extra_arguments: [])
  end

  def stop_supervisor(timeout \\ 5000) do
    Supervisor.stop(__MODULE__, :normal, timeout)
  end

  defp start({name, port}) do
    name = {:via, Registry, {RecRegistry, name}}

    DynamicSupervisor.start_child(@supervisor_name, {LiveTrack.ReceiverWorker, {name, port}})
  end

  def stop(name) do
    case find(name) do
      {:ok, pid} ->
        DynamicSupervisor.terminate_child(@supervisor_name, pid)

      e ->
        e
    end
  end

  defp find_or_create(name, port) do
    case Registry.lookup(RecRegistry, name) do
      [] -> {:ok, _pid} = start({name, port})
      [{pid, nil}] -> {:ok, pid}
    end
  end

  defp find(name) do
    case Registry.lookup(RecRegistry, name) do
      [] -> {:error, :not_exist}
      [{pid, nil}] -> {:ok, pid}
    end
  end
end
