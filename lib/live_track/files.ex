defmodule LiveTrack.Files do

  def ls(expand \\ "", base_path \\ "/priv/")
  def ls(expand, base_path) do
    if String.match?(expand, ~r/\.\./) do
      {:error, "минимальный уровень директории"}
    else
      path = File.cwd! |> Path.join(base_path) |> Path.join(expand)

      path
      |> File.ls!()
      |> Enum.map(fn file_path ->
        expanded = Path.join(path, file_path)
        %{type: read_type(expanded), ctime: read_ctime(expanded), size: read_size(expanded), filename: file_path}
      end)
    end
  rescue
    _e -> {:error, "cant execute"}
  end

  def read(expand, base_path \\ "/priv/") do
    File.cwd!
    |> Path.join(base_path)
    |> Path.join(expand)
    |> File.read!()
  rescue
    _e -> {:error, "cant read file"}
  end

  def read_type(path) do
    if String.match?(path, ~r/\.\w+$/) do
      "file"
    else
      "directory"
    end
  end

  def read_size(path) do
    {size, _} = System.cmd("du", ["-hs", path])
    size
    |> String.split("\t")
    |> hd()
  rescue
    _e -> "unknown"
  end

  def read_ctime(path) do
    System.cmd("find", [path, "-type", "f", "-printf",  ~s|"%T+ %p"|])
    |> elem(0)
    |> String.split(~s|\"\"|)
    |> Enum.map(&String.replace(&1, "\"", ""))
    |> Enum.max_by(& &1)
    |> String.split(" ")
    |> hd
    |> String.replace("+", "T")
    |> NaiveDateTime.from_iso8601!
    |> NaiveDateTime.add(3 * 60 * 60, :second)
  rescue
    _e -> "unknown"
  end
end
