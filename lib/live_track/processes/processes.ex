defmodule LiveTrack.Server.Processes do
  alias LiveTrack.{RecRegistry, SimpleRegistry}

  def ls(expand), do: LiveTrack.Files.ls(expand, "lib/live_track/protocols")
  def read(expand), do: LiveTrack.Files.read(expand, "lib/live_track/protocols")

  def create_port(name, port), do: LiveTrack.ReceiverSupervisor.create(name, port)
  def stop_port(name),         do: LiveTrack.ReceiverSupervisor.stop(name)

  def list_ports do
    RecRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Enum.map(&get_port/1)
  end

  def list_buffer do
    SimpleRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Enum.map(&get_buffer/1)
  end

  defp get_port({name, pid, _}) do
    %{
      name: name,
      state: get_info(pid)
    }
  rescue
    e -> {:error, e}
  end

  defp get_buffer({name, pid, _}) do
    %{
      name: name,
      worker: get_gen_server(pid),
      state: get_info(pid)
    }
  rescue
    e -> {:error, e}
  end

  defp get_gen_server(pid) do
    GenServer.call(pid, :check_state)
  end

  defp get_info(pid) do
    Process.info(pid)
    |> Keyword.delete(:current_function)
    |> Keyword.delete(:initial_call)
    |> Keyword.delete(:links)
    |> Keyword.delete(:dictionary)
    |> Keyword.delete(:group_leader)
  end
end
