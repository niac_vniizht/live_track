# defmodule LiveTrack.Receiver do
#   @moduledoc """
#   Модуль описывающий организацию приема TCP пакетов, а также формирования и разрыва сессий.
#   """
#   require Logger
#   alias LiveTrack.Helpers.Utils

#   @doc """
#   Init - запускает процесс на заданном порту
#   Конфигурация сервера - бинарные пакеты, пассивный режим (общение исключительно в ручном режиме), адрес и порт могут быть переиспользованы
#   """
#   @spec init(char) :: no_return
#   def init(port) do
#     opts = [:binary, active: false, reuseaddr: true]

#     {:ok, socket} = :gen_tcp.listen(port, opts)
#     Logger.info("Сервер принимает tcp соединения на порту:  #{port}", application: :tcp_receiver)

#     loop_acceptor(socket)
#   end

#   @doc """
#   Функция loop_acceptor отлавливает соединения с сервером и использует Task Supervisor
#   для асинхронного подключения машин (контролирует максимальное число одновременно работающих машин)
#   Если устройство подключается, то запускает функцию serve (через start_communication)
#   """
#   @spec loop_acceptor(port | {:"$inet", atom, any}) :: no_return
#   def loop_acceptor(socket) do
#     {:ok, client_socket} = :gen_tcp.accept(socket)

#     with {:ok, pid} <- start_communication(client_socket),
#          :ok <- :gen_tcp.controlling_process(client_socket, pid) do
#       loop_acceptor(socket)
#     else
#       _ ->
#         Process.sleep(100)
#         loop_acceptor(socket)
#     end
#   end

#   defp start_communication(client_socket),
#     do: Task.Supervisor.start_child(LiveTrack.TaskSupervisor, fn -> serve(client_socket) end)

#   @doc """
#   Функция servе
#   Принимает данные от устройства, начинает построчное чтение этих данных
#   Обслуживание начинается с приемки авторизационных параметров и первого пакета, чтобы определить какой механизм (конечный автомат) примет процесс предачи данных и будет их обрабатывать
#   Если сообщение не может быть обработано - сессия разрывается, данные не сохраняются.
#   """
#   @spec serve(port | {:"$inet", atom, any}) :: :closed | :connected
#   def serve(socket) do
#     case :inet.peername(socket) do
#       {:ok, {ip, port}} ->
#         LiveTrack.Logs.do_log(%{
#           log_type: "info",
#           state: "connected",
#           version: "_",
#           message: inspect(%{ip: ip, port: port}, limit: :infinity)
#         })

#       _ ->
#         LiveTrack.Logs.do_log(%{
#           log_type: "warn",
#           state: "connected",
#           version: "_",
#           message: inspect(%{ip: "NA", port: "NA"}, limit: :infinity)
#         })

#         LiveTrack.Logs.do_log(
#           %{
#             log_type: "warn",
#             state: "connected",
#             version: "_",
#             message: inspect(%{ip: "NA", port: "NA"}, limit: :infinity)
#           },
#           "error"
#         )
#     end

#     %{socket: socket}
#     |> handle_auth_info()
#     |> connect_to_fsm()
#   end

#   @doc """
#   Принимает авторизационные параметры
#   """
#   def handle_auth_info(%{socket: socket}) do
#     set_buffer(socket, LiveTrack.Helpers.Utils.read_line!(socket))
#   rescue
#     e ->
#       IO.puts(IO.ANSI.yellow() <> inspect(e) <> IO.ANSI.default_color())
#       send_error(socket, "Socket closed due unknown error")
#   end

#   def set_buffer(socket, line) do
#     [h | t] =
#       line
#       |> Utils.split_line()

#     case LiveTrack.Validator.set_protocol_buffer(h, [socket: socket]) do
#       {:ok, buffer} ->
#         log = %{
#           log_type: "info",
#           state: "authenticated",
#           version: "hello",
#           message: inspect(%{line: line, buffer: buffer}, limit: :infinity)
#         }

#         prot_dir = LiveTrack.Helpers.Utils.get_dir(buffer)

#         LiveTrack.Logs.do_log(log, "log", prot_dir)

#         try do
#           LiveTrack.Logs.do_log(log, buffer.auth.body.imei, prot_dir)
#         rescue
#           _ -> :ok
#         end

#         t
#         |> Enum.reduce(buffer, fn string, buffer ->
#           buffer = apply(buffer.type, :add, [buffer, string])
#           Utils.run_buffer(socket, buffer)
#         end)
#         |> case do
#           %{finish: true} ->
#             buffer = Utils.run_buffer(socket, buffer)
#             apply(buffer.type, :finalize, [buffer, socket])
#             :ok

#           buffer ->
#             %{socket: socket, buffer: buffer}
#         end

#       {:error, e} ->
#         send_error(socket, e)
#     end
#   end

#   @doc """
#   Передает процесс приемки данных выбранному исходя из первого сообщения автомату
#   """
#   def connect_to_fsm(%{socket: socket, buffer: buffer}) do
#     {:connected, pid} = LiveTrack.Supervisor.create(socket, buffer)
#     :ok = :gen_tcp.controlling_process(socket, pid)
#   rescue
#     e ->
#       IO.inspect(e)
#       send_error(socket, "Server error\r\n")
#   end

#   def connect_to_fsm(any), do: any

#   def send_error(socket, msg) do
#     LiveTrack.Logs.do_log(%{log_type: "warn", state: "error", version: "unknown", message: msg})
#     :gen_tcp.send(socket, "#{msg}\n")
#     :gen_tcp.close(socket)
#     :closed
#   end
# end
