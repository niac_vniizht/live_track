defmodule LiveTrack.Helpers.Utils do
  @moduledoc """
  Вспомогательные функции
  """
  require Logger
  def send_response(socket, {:ok, data}), do: send_response(socket, data)

  def send_response(socket, data) do
    msg = Poison.encode!(%{byte_size: bz(data)})
    send_message(socket, :simple, msg)

    socket
  end

  def bz(str) do
    str
    |> byte_size
  end

  def read_response(line) do
    line
    |> String.trim()
    |> Poison.decode!()
  rescue
    _e -> false
  end

  @doc """
  Записывает данные в файл
  """
  def write_file(data_strings, auth, prot_dir \\ "") do
    dt = DateTime.utc_now |> DateTime.to_naive
    date = dt |> NaiveDateTime.to_date |> Date.to_string

    prot_path = if prot_dir != "" do
      "#{File.cwd!()}/priv/#{prot_dir}"
    else
      "#{File.cwd!()}/priv"
    end

    data_path = "#{prot_path}/machine_data"


    path = "#{data_path}/[#{date}]"

    file_name = "#{path}/#{auth}.txt"

    if !File.exists?(file_name) do
      File.mkdir(prot_path)
      File.mkdir(data_path)
      File.mkdir(path)
      File.touch!(file_name)
    end

    Enum.map(data_strings, fn data ->
      File.write!(file_name, "[#{dt}]: #{data}\n", [:append])
    end)
  end

  def take_safely(list, func) do
    case list do
      [] -> []
      list when is_list(list) -> func.(list)
    end
  end

  def slice_data(data_string) when is_binary(data_string) do
    data_string
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> Enum.filter(& &1)
  end

  def slice_data(any), do: any

  def split_line(nil), do: []

  def split_line(line) do
    if String.match?(line, ~r/\n/) do
      list =
        line
        |> String.split("\n")
        |> Enum.reject(&(&1 == ""))
        |> Enum.map(& &1 |> String.replace("\r", "") |> Kernel.<>("\n"))

      if String.ends_with?(line, "\n") do
        list
      else
        [last | rev] = Enum.reverse(list)
        [String.trim(last, "\n") | rev] |> Enum.reverse()
      end
    else
      [line]
    end
  end

  def is_message(message, msg) when is_binary(message),
    do:
      message
      |> String.replace("\n", "")
      |> String.replace("\r", "")
      |> String.downcase()
      |> Kernel.==(String.downcase(msg))

  def is_message({:ok, message}, msg),
    do: message |> String.replace("\n", "") |> String.replace("\r", "") |> Kernel.==(msg)

  def is_message(_, _), do: false

  def add_if(str, arr) when is_list(arr) do
    arr ++ [str]
  end

  def add_if(str, _arr) do
    [str]
  end

  @doc """
  считывает строку, если доступна

      iex> LiveTrack.Helpers.Utils.read_line!(socket)
      data
      iex> LiveTrack.Helpers.Utils.read_line!(closed_socket)
      ** (throw) "could not be recognized"

  """
  def read_line!(socket) do
    case read_line(socket) do
      {:ok, data} -> data
      :closed -> throw("could not be recognized")
    end
  end

  @doc """
  считывает строку, если доступна

      iex> LiveTrack.Helpers.Utils.read_line!(socket)
      {:ok, data}
      iex> LiveTrack.Helpers.Utils.read_line!(closed_socket)
      :closed

  """
  @spec read_line(any(), integer()) :: :closed | {:ok, any}
  def read_line(socket, timeout \\ 15) do
    case :gen_tcp.recv(socket, 0, :timer.minutes(timeout)) do
      {:ok, data} -> {:ok, data}
      {:error, :timeout} -> {:ok, nil}
      {:error, :closed} -> :closed
      critical -> error_close(critical)
    end
  end

  @doc """
  Устройства на данный момент не принимают ответных сообщений
  """
  def send_message(socket, :zip, _message) do
    # resp = Poison.encode!(%{message: inspect(message)}) |> :zlib.gzip()
    # send_message(socket, :simple, resp)
    socket
  end

  def send_message(socket, :mapify, _message) do
    # resp = Poison.encode!(%{message: inspect(message)})
    # send_message(socket, :simple, resp)
    socket
  end

  def send_message(socket, :simple, _message) do
    # :gen_tcp.send(socket, message <> "\n")

    socket
  end

  def error_close(msg) do
    Logger.debug(msg, application: :tcp_receiver)
    :closed
  end

  def auth_to_service_name(auth), do: auth |> inspect() |> String.to_atom()

  def run_buffer(socket, %{opts: opts} = buffer) do
    buffer
    |> do_write_file()
    |> do_log(opts[:log])
    |> do_respond(socket, opts[:respond])
    |> do_insert()
    |> clear_log()
    |> Map.put(:respond, nil)
    |> Map.put(:messages, [])
    |> Map.put(:data_strings, [])
  end

  def do_respond(%{respond: nil} = buffer, _socket, true) do
    buffer
  end

  def do_respond(%{respond: respond} = buffer, socket, true) do
    :gen_tcp.send(socket, respond)
    buffer
  end

  def do_respond(buffer, _socket, _), do: buffer

  def do_log(%{log: nil} = buffer, true), do: buffer

  def do_log(%{log: log} = buffer, true) do
    dir = get_dir(buffer)
    Enum.each(log_files(log, buffer), fn f ->
      try do
        LiveTrack.Logs.do_log(log, f, dir)
      rescue
        _ -> :ok
      end
    end)
    buffer
  end

  def do_log(%{logs: []} = buffer, true), do: buffer

  def do_log(%{logs: logs} = buffer, true) do
    dir = get_dir(buffer)
    Enum.map(logs, fn log ->
      Enum.each(log_files(log, buffer), fn f ->
        try do
          LiveTrack.Logs.do_log(log, f, dir)
        rescue
          _ -> :ok
        end
      end)
    end)

    buffer
  end

  def do_log(buffer, _), do: buffer

  def log_files(%{log_type: "error"}, %{auth: %{body: %{imei: imei}}}), do: ["error", "err_#{imei}"]
  def log_files(%{log_type: "error"}, _), do: ["error"]
  def log_files(%{log_type: "warn"}, %{auth: %{body: %{imei: imei}}}), do: ["log", "err_#{imei}"]
  def log_files(%{log_type: "warn"}, _), do: ["log"]
  def log_files(_, %{auth: %{body: %{imei: imei}}}), do: ["log", "#{imei}"]
  def log_files(_, _), do: ["log"]

  def get_dir(%{dir: dir}) when is_binary(dir), do: dir
  def get_dir(_), do: ""

  def file_prefix(%LiveTrack.Structures.Scout2Buffer{}), do: "scout_2_"
  # def file_prefix(%LiveTrack.Structures.WialonBuffer{}), do: "wialon_"
  def file_prefix(%LiveTrack.Structures.NiacBuffer{}), do: "niac_"
  def file_prefix(%LiveTrack.Structures.MvpsBuffer{}), do: "mvps_"
  def file_prefix(_), do: ""

  def do_write_file(%{data_strings: []} = buffer), do: buffer

  def do_write_file(%{data_strings: data_strings, auth: %{body: %{imei: imei}}} = buffer) do
    if Application.get_env(:server, :env) != :test, do: LiveTrack.Helpers.Utils.write_file(data_strings, imei, get_dir(buffer))
    buffer
  end

  def do_write_file(buffer), do: buffer

  def do_insert(buffer) do
    if Application.get_env(:server, :env) != :test do
      apply(buffer.type, :send_data, [buffer])
    end
    buffer
  rescue
    _e ->
      buffer
  end



  def clear_log(%{logs: _} = buffer), do: Map.put(buffer, :logs, [])
  def clear_log(%{log: _} = buffer), do: Map.put(buffer, :log, nil)
  def clear_data_strings(%{data_strings: _} = buffer), do: Map.put(buffer, :data_strings, [])
  def clear_data_strings(buffer), do: buffer
end
