defmodule LiveTrack.Writters.Helpers do
  @moduledoc """
  Запись данных в базу производится исходя из определенных блоков данных
  """

  alias Helpers.SqlSupQuery

  def update_or_insert_last_info(params, db_key) do
    dt = DateTime.utc_now |> DateTime.to_naive

    add_update_last_location(params, dt, db_key)
    add_update_last_commun(params, dt, db_key)
    # send_machines(params)
    :ok
  end

  def send_machines(%{imei: imei} = params) do
    LiveTrack.Client.send_message_to_chat(Jason.encode!(params), "machines", "#{imei}", :ws, %{})
  end

  def send_machines(params) do
    LiveTrack.Client.send_message_to_chat(Jason.encode!(params), "machines", "unknown", :ws, %{})
  end

  def add_update_last_location(%{imei: imei, lat: lat, lon: lon}, dt, db_key) do
    case SqlSupQuery.run_query("select * from last_location where imei = #{imei} limit 1", db_key) do
      [loc] when is_map(loc) ->
        SqlSupQuery.run_query("update last_location set dt = '#{dt}', lat = #{lat}, lon = #{lon} where imei = #{imei}", db_key)
      _ -> SqlSupQuery.insert(%{imei: imei, dt: dt, lat: lat, lon: lon}, "last_location", db_key)
    end
  end
  def add_update_last_location(%{imei: imei}, dt, db_key) do
    case SqlSupQuery.run_query("select * from last_location where imei = #{imei} limit 1", db_key) do
      [loc] when is_map(loc) ->
        SqlSupQuery.run_query("update last_location set dt = '#{dt}' where imei = #{imei}", db_key)
      _ -> SqlSupQuery.insert(%{imei: imei, dt: dt}, "last_location", db_key)
    end
  end
  def add_update_last_location(_, _, _), do: :ok
  def add_update_last_commun(%{imei: imei}, dt, db_key) do
    case SqlSupQuery.run_query("select * from last_communication where imei = #{imei} limit 1", db_key) do
      [loc] when is_map(loc) ->
        SqlSupQuery.run_query("update last_communication set dt = '#{dt}' where imei = #{imei}", db_key)
      _ -> SqlSupQuery.insert(%{imei: imei, dt: dt}, "last_communication", db_key)
    end
  end
  def add_update_last_commun(_, _, _), do: :ok

  def update_or_insert_action(action, len, %{imei: imei}, db_key) do
    date = Date.utc_today()
    action = Atom.to_string(action) |> String.replace("_block", "")

    case get_info(action, imei, date, db_key)  do
      [%{count: count, id: id}] ->
        """
        update stat_info
        set count = #{count + len}
        where id = #{id}
        """
        |> SqlSupQuery.run_query(db_key)

      _ ->
        SqlSupQuery.insert(%{imei: imei, type_str: action, date: date, count: len}, "stat_info", db_key)
    end
  end

  def get_info(action, imei, date, db_key) do
    """
    select *
    from stat_info
    where imei = #{imei} and type_str = '#{action}' and date = '#{date}'
    order by date desc
    limit 1
    """
    |> SqlSupQuery.run_query(db_key)
  rescue
    _e -> nil
  end

  def get_table(:doors_block),       do: "doors_info"
  def get_table(:geo_data_block),    do: "geo_info"
  def get_table(:rssi_block),        do: "rssi"
  def get_table(:telemetry_block),   do: "telemetry"
  def get_table(:temperature_block), do: "temperature"
end
