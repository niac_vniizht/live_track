defmodule LiveTrack.Validator do
  @moduledoc false
  require Logger


  @doc """
  LiveTrack.Validator.set_protocol_buffer("2,868136035385668,20313343414D500700420037")

  """
  def set_protocol_buffer(line, attrs \\ []) do
    cond do
      is_comm(line) ->
        LiveTrack.Structures.CommBuffer.new(line, attrs)

      is_niac(line) ->
        LiveTrack.Structures.NiacBuffer.new(line)

      is_wialon(line) ->
        LiveTrack.Structures.WialonBuffer.new(line)

      is_mvps(line) ->
        LiveTrack.Structures.MvpsBuffer.new(line)

      # is_scout2(line) ->
      #   LiveTrack.Structures.Scout2Buffer.new(line)

      true ->
        {:error, :unknown_buffer_type}
    end
  end


  def is_comm(line), do: LiveTrack.Structures.CommBuffer.is_valid_login(line)
  def is_wialon(line), do: LiveTrack.Structures.WialonBuffer.is_valid_login(line)
  def is_niac(line), do: LiveTrack.Structures.NiacBuffer.is_valid_login(line)
  def is_mvps(line), do: LiveTrack.Structures.MvpsBuffer.is_valid_login(line)
  def is_scout2(line), do: LiveTrack.Structures.Scout2Buffer.is_valid_login(line)

  # def is_scout(line), do: LiveTrack.Structures.ScoutBuffer.is_valid_login(line)

  # def validation_status(line, :simple) do
  #   sliced_data = TcpReceiver.Helpers.slice_data(line)

  #   cond do
  #     TcpReceiver.Helpers.is_message(line, "0") ->
  #       :close_message

  #     sliced_data |> length |> Kernel.>=(4) ->
  #       check_summs(sliced_data)

  #     true ->
  #       :invalid
  #   end
  # end

  # def check_summs(list) do
  #   [summ | pack] = list |> :lists.reverse()
  #   summ = String.to_integer(summ)
  #   pack_summ = pack |> :lists.reverse() |> Enum.join(",") |> :erlang.crc16()

  #   if(summ == pack_summ,
  #     do: {:valid, :sum_eq, Enum.join(list, ",")},
  #     else: {:valid, :pack_broken}
  #   )
  # rescue
  #   _ -> :invalid
  # end
end
