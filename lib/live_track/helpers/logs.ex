defmodule LiveTrack.Logs do
  require Logger

  def do_log(any, imei \\ "log", prot_dir \\ "")

  def do_log(
        %{log_type: type, protocol_name: protocol_name, state: "end", version: version},
        imei,
        prot_dir
      ) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{protocol_name}::#{version}::end]
    -----------------------------------------------------------------
    """

    try_put_msg(msg, imei, prot_dir)
  end

  def do_log(%{log_type: type, state: "end"}, imei, prot_dir) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} end]
    -----------------------------------------------------------------
    """

    try_put_msg(msg, imei, prot_dir)
  end

  def do_log(%{log_type: type, state: "connected", message: message}, imei, prot_dir) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    -----------------------------------------------------------------
    [#{date_time} #{log_type} connected]
    #{message}
    """

    try_put_msg(msg, imei, prot_dir)
  end

  def do_log(
        %{
          log_type: type,
          protocol_name: protocol_name,
          state: state,
          version: version,
          message: message
        },
        imei,
        prot_dir
      ) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{protocol_name}::#{version}::#{state}]
    #{message}
    """

    try_put_msg(msg, imei, prot_dir)
  end

  def do_log(
        %{
          log_type: type,
          protocol_name: protocol_name,
          message: message
        },
        imei,
        prot_dir
      ) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{protocol_name}]
    #{message}
    """

    try_put_msg(msg, imei, prot_dir)
  end

  def do_log(%{log_type: type, state: state, message: message}, imei, prot_dir) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{state}]
    #{message}
    """

    try_put_msg(msg, imei,prot_dir)
  end

  def try_put_msg(msg, imei \\ "log", prot_dir \\ "") do
    if Application.get_env(:server, :env) !== :test do
      if imei == "log" do
        IO.puts(msg)
      end

      write_log(msg, imei, prot_dir)
    end
  rescue
    e ->
      Logger.error(e)
  end

  def write_log(msg, imei \\ "log", prot_dir \\ "") do
    dt = DateTime.utc_now |> DateTime.to_naive |> NaiveDateTime.to_date |> Date.to_string

    prot_path = if prot_dir != "" do
      "#{File.cwd!()}/priv/#{prot_dir}"
    else
      "#{File.cwd!()}/priv"
    end

    logs_path = "#{prot_path}/logs"

    path = "#{logs_path}/[#{dt}]"

    file_name = "#{path}/#{imei}.log"

    if !File.exists?(file_name) do
      File.mkdir(prot_path)
      File.mkdir(logs_path)
      File.mkdir(path)
      File.touch!(file_name)
    end

    File.write!(file_name, msg, [:append])
  end

  defp get_log_type("info"), do: "#{IO.ANSI.green()} INFO #{IO.ANSI.reset()}"
  defp get_log_type("warn"), do: "#{IO.ANSI.yellow()} WARNING #{IO.ANSI.reset()}"
  defp get_log_type("error"), do: "#{IO.ANSI.red()} ERROR #{IO.ANSI.reset()}"
end
