defmodule LiveTrack.Query do
  require Logger

  def insert(map, table, db_atom) when is_map(map), do: insert([map], table, db_atom)

  def insert(list, table, db_atom) when is_list(list) do
    db_params = Server.GStore.get_db("GStore", db_atom)
    Server.SqlSup.insert_chunked(db_params, list, table)
  end
end
