defmodule Mvps.Helpers do
  @moduledoc """
  Вспомогательные функции
  """
  require Logger
  def send_response(socket, {:ok, data}), do: send_response(socket, data)

  def send_response(socket, data) do
    msg = Poison.encode!(%{byte_size: bz(data)})
    send_message(socket, :simple, msg)

    socket
  end

  def bz(str) do
    str
    |> byte_size
  end

  def read_response(line) do
    line
    |> String.trim()
    |> Poison.decode!()
  rescue
    _e -> false
  end

  @doc """
  Записывает данные в файл и оповещает всех пользователей подписанных на обновление машины
  """
  def write_file(%{auth: auth, data_strings: data_strings}) do
    %{end_sign: _end_sign, imei: imei, key: key, num: _num, pream: _pream} =
      Mvps.Packages.Parcer.parce_pack(auth)

    device = "7_#{imei}_#{Enum.join(:binary.bin_to_list(key), ",")}"

    dt_start = DateTime.utc_now |> DateTime.to_naive |> NaiveDateTime.to_date |> Date.to_string
    file_name = "#{File.cwd!()}/priv/machine_data/[#{dt_start}]/#{device}.txt"
    dt = DateTime.utc_now |> DateTime.to_naive |> NaiveDateTime.to_string

    # ServerWeb.HelperFuncs.broadcast("devices", {"new_records", device, length(data_strings)})

    if !File.exists?(file_name) do
      File.mkdir("#{File.cwd!()}/priv/machine_data/[#{dt_start}]")
      File.touch!(file_name)
    end

    Enum.map(data_strings, fn data ->
      data = :binary.bin_to_list(data) |> Enum.join(",")
      # ServerWeb.HelperFuncs.broadcast("device:#{device}", {"push_new", "[#{dt}]", "#{data}"})
      File.write!(file_name, "[#{dt}]: #{data}\n", [:append])
    end)
  end

  def write_log(msg, imei \\ "log") do
    dt = DateTime.utc_now |> DateTime.to_naive |> NaiveDateTime.to_date |> Date.to_string
    path = "#{File.cwd!()}/priv/logs/#{dt}"
    file_name = "#{path}/#{imei}.log"

    if !File.exists?(file_name) do
      File.mkdir_p(path)
      File.touch!(file_name)
    end

    File.write!(file_name, msg, [:append])
  end

  def take_safely(list, func) do
    case list do
      [] -> []
      list when is_list(list) -> func.(list)
    end
  end

  def slice_data(data_string) when is_binary(data_string) do
    data_string
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> Enum.filter(& &1)
  end

  def slice_data(any), do: any

  def is_message(message, msg) when is_binary(message),
    do:
      message
      |> String.replace("\n", "")
      |> String.replace("\r", "")
      |> String.downcase()
      |> Kernel.==(String.downcase(msg))

  def is_message({:ok, message}, msg),
    do: message |> String.replace("\n", "") |> String.replace("\r", "") |> Kernel.==(msg)

  def is_message(_, _), do: false

  def add_if(str, arr) when is_list(arr) do
    arr ++ [str]
  end

  def add_if(str, _arr) do
    [str]
  end

  def read_line!(socket) do
    case read_line(socket) do
      {:ok, data} -> data
      {:error, :timeout} -> {:error, :timeout}
      :closed -> throw("could not be recognized")
    end
  end

  @spec read_line(any()) :: :closed | {:error, :timeout} | {:ok, any}
  def read_line(socket) do
    case :gen_tcp.recv(socket, 0, :timer.minutes(1)) do
      {:ok, data} -> {:ok, data}
      {:error, :closed} -> :closed
      {:error, :timeout} -> {:error, :timeout}
      critical -> error_close(critical)
    end
  end

  @doc """
  Устройства на данный момент не принимают ответных сообщений
  """
  def prepare_message(:zip, message),
    do: Poison.encode!(%{message: inspect(message)}) |> :zlib.gzip() |> Kernel.<>("\n")

  def prepare_message(:mapify, message),
    do: Poison.encode!(%{message: inspect(message)}) <> "\n"

  def prepare_message(:simple, message),
    do: message <> "\n"

  # def send_message(socket, type, message) do
  #   resp = prepare_message(type, message)
  #   :gen_tcp.send(socket, resp <> "\n")
  #   socket
  # end

  def send_message(socket, :zip, _message) do
    # resp = Poison.encode!(%{message: inspect(message)}) |> :zlib.gzip()
    # send_message(socket, :simple, resp)
    socket
  end

  def send_message(socket, :mapify, _message) do
    # resp = Poison.encode!(%{message: inspect(message)})
    # send_message(socket, :simple, resp)
    socket
  end

  def send_message(socket, :simple, _message) do
    # :gen_tcp.send(socket, message <> "\n")

    socket
  end

  def error_close(msg) do
    Logger.debug(inspect(msg, limit: :infinity), application: :tcp_receiver)
    :closed
  end

  def auth_to_service_name(auth), do: auth |> inspect() |> String.to_atom()
end
