defmodule Mvps.Validators do
  @moduledoc false

  @doc """
  Сатус валидации

      iex> Mvps.Validators.validation_status("123321,421412,412,3357427843\n", :simple)
  """
  def validation_status(_line, :simple), do: :valid
  def validation_status(line, :simplex), do: {:valid, :sum_eq, line}

  # def validation_status(line, :simple) do
  #   sliced_data = Mvps.Helpers.slice_data(line)

  #   cond do
  #     Mvps.Helpers.is_message(line, "0") ->
  #       :close_message

  #     sliced_data |> length |> Kernel.>=(4) ->
  #       check_summs(sliced_data)

  #     true ->
  #       :invalid
  #   end
  # end

  def check_summs(list) do
    [summ | pack] = list |> :lists.reverse()
    summ = String.to_integer(summ)
    pack_summ = pack |> :lists.reverse() |> Enum.join(",") |> :erlang.crc32()

    if(summ == pack_summ,
      do: {:valid, :sum_eq, Enum.join(list, ",")},
      else: {:valid, :pack_broken}
    )
  rescue
    _ -> :invalid
  end
end
