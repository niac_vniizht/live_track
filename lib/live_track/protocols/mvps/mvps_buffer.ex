defmodule LiveTrack.Structures.MvpsBuffer do
  @behaviour LiveTrack.BufferBehaviour
  alias Mvps.Packages.{Login, DataPackage}
  alias Helpers.SqlSupQuery
  alias LiveTrack.Packages.Helpers

  defstruct version: nil,
            type: __MODULE__,
            data_strings: [],
            messages: [],
            opts: nil,
            auth: nil,
            log: nil,
            respond: nil,
            dir: "mvps",
            db_key: :auth_rec,
            finish: false

  def pack_types do
    [
      L: %{
        type: "Login",
        structure: Login
      },
      P: %{
        type: "Data Pack",
        structure: DataPackage
      }
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: false, validate: false], opts)
    {type, parsed_message} = read_pack(line)

    protocol_version = "20220314"

    case read_message(parsed_message, protocol_version, type) do
      {:ok, %{finish: finish} = parsed_message} ->
        res =
          %__MODULE__{
            version: protocol_version,
            auth: parsed_message,
            finish: finish,
            opts: opts
          }
          |> Helpers.put_log(parsed_message, protocol_version)
          |> Helpers.put_respond(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    _e ->
      {:error, "ошибка при создании буфера"}
  end

  def add(%__MODULE__{version: protocol_version, messages: messages, auth: _auth, data_strings: data_strings} = mod, line) do
    {type, parsed_message} = read_pack(line)

    case read_message(parsed_message, protocol_version, type) do
      {:ok, parsed_message} ->
        line = :binary.bin_to_list(line) |> Enum.join(",")
        %__MODULE__{mod | messages: messages ++ [transform_for_insert(parsed_message)], data_strings: data_strings ++ [line]}
        |> Helpers.put_log(parsed_message, protocol_version)
        |> Helpers.put_respond(parsed_message)

      _msg ->
        %__MODULE__{mod | finish: true}
    end
  end

  def read_pack(line) do
    case line do
      <<161, _rest::binary>> -> {:L, line}
      <<162, _rest::binary>> -> {:P, line}
      <<163, _rest::binary>> -> {:F, line}
      _ ->
        LiveTrack.Logs.do_log(%{log_type: "warn", state: "finalization", version: "20220314", message: inspect(line, pretty: true, limit: :infinity)}, "error", "mvps")
        {:F, line}
    end
  end

  def read_message(msg, protocol_version, type) do
    case is_valid_type(type) do
      true ->
        options = %{prot_version: protocol_version}
        %{structure: struct} = Keyword.get(pack_types(), type)
        parsed_message = apply(struct, :new, [msg, options])

        {:ok, parsed_message}

      false -> {:error, msg}
    end
  end


  def transform_for_insert(parsed_packs), do: parsed_packs

  def send_data(%__MODULE__{auth: %{body: auth_body}, db_key: db_key, messages: messages}) do
    %{end_sign: _end_sign, imei: imei, key: key, num: num, pream: _pream} = auth_body

    auth_params = %{imei: imei, key: :binary.bin_to_list(key), num: num}

    messages
    |> Enum.map(fn %{body: %{blocks: blocks}} ->
      Map.to_list(blocks)
    end)
    |> List.flatten()
    |> Enum.group_by(&elem(&1, 0), &elem(&1, 1))
    |> Enum.map(fn {k, v} ->
      v = List.flatten(v)
      {
        k,
        %{
          count: Enum.reduce(v, 0, &(&1[:count] + &2)),
          pack: Enum.reduce(v, [], &(&1[:pack] ++ &2))
        }
      }
    end)
    |> Enum.map(& send_blocks(&1, num, auth_params, db_key))
  end

  @spec send_blocks(any, any, %{:imei => any, optional(any) => any}, any) :: nil | :ok
  def send_blocks({block_name, data}, _num, auth, db_key) do
    table = get_table(block_name)
    Enum.map(data[:pack], &(Map.merge(&1, auth) |> Map.put(:dt, DateTime.utc_now |> DateTime.to_naive)))
    |> SqlSupQuery.insert(table, db_key)

    LiveTrack.Writters.Helpers.update_or_insert_action(block_name, length(data[:pack]), auth, db_key)
    LiveTrack.Writters.Helpers.update_or_insert_last_info(Map.merge(get_last_map(data), auth), db_key)
    send_socket_data_list(data, auth)
    :ok
  end

  def send_blocks(data, _num, %{imei: imei}, _) do
    make_log(%{
      log_type: "warn", state: "sending", version: "20220314",
      message: inspect(%{imei: imei, error: "unknown data type", data: data}, limit: :infinity)
    }, imei)
  end

  def send_socket_data_list(%{imei: imei} = params) do
    LiveTrack.Client.send_message_to_chat(Jason.encode!(params), "machines", "#{imei}", :ws, %{})
  end
  def send_socket_data_list(%{pack: lpack}, auth) when is_list(lpack) do
    lpack
    |> Enum.sort_by(& (&1.pack_dt || ~N[2000-07-14 13:52:32]), {:asc, NaiveDateTime})
    |> Enum.map(fn data ->
      data
      |> Map.merge(auth)
      |> send_socket_data_list
    end)
  end

  def get_last_map(%{pack: lpack}) when is_list(lpack) do
    lpack
    |> Enum.sort_by(& (&1.pack_dt || ~N[2000-07-14 13:52:32]), {:asc, NaiveDateTime})
    |> hd()
  rescue
    _e -> %{}
  end



  def finalize(%__MODULE__{messages: []}, socket) do
    :gen_tcp.close(socket)
  end

  def finalize(%__MODULE__{messages: messages}, socket) do
    :gen_tcp.close(socket)

    LiveTrack.Logs.do_log(%{
      log_type: "warn",
      state: "has_messages",
      message: inspect(messages, limit: :infinity)
    })
  end

  def is_valid(line) do
    ["", type, _raw_message] = String.split(line, "#")

    is_valid_type(type)
  rescue
    _e -> false
  end

  def is_valid_login(line) do
    {type, _pack} = read_pack(line)

    type == :L
  rescue
    _e -> false
  end

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())

  def get_table(:doors_block),       do: "doors_info"
  def get_table(:geo_data_block),    do: "geo_info"
  def get_table(:rssi_block),        do: "rssi"
  def get_table(:telemetry_block),   do: "telemetry"
  def get_table(:temperature_block), do: "temperature"


  def make_log(map, imei) do
    LiveTrack.Logs.do_log(map, "error_#{imei}", "mvps")
    LiveTrack.Logs.do_log(map, "error", "mvps")
    LiveTrack.Logs.do_log(map, "log", "mvps")
  end
end
