defmodule Mvps.Packages.Login do

  defstruct ptype: "L",
            body: nil,
            status: nil,
            log: nil,
            respond: nil,
            finish: false

  def new(msg, %{prot_version: prot_version}) do
    with true <- is_binary(msg),
         %{pream: _, num: _, imei: imei, key: _, end_sign: _} = body <- body(msg),
         {:check_imei_len, 15, _} <- {:check_imei_len, String.length("#{imei}"), imei} do

      log = %{
        log_type: "info",
        state: "authenticated",
        version: prot_version,
        message: inspect(%{auth: msg, imei: imei}, limit: :infinity)
      }

      resp = case Mvps.Validators.validation_status(msg, :simplex) do
        {_, :sum_eq, _line} -> Mvps.Helpers.prepare_message(:simple, "packs fetched succesfully")
        # {_, :pack_broken} -> Mvps.Helpers.prepare_message(:simple, "packs crc32 error")
        _ -> nil
      end

      %__MODULE__{body: body, respond: resp, log: log}
    else
      {:check_imei_len, len, imei} ->
        log = %{
          log_type: "warn",
          state: "authenticated",
          version: prot_version,
          message: inspect(
            %{auth: msg, imei: imei, error: "imei should be 15 symbols got #{len}"},
            limit: :infinity
          )
        }

        resp = Mvps.Helpers.prepare_message(:mapify, "imei should be 15 symbols got #{len}")

        %__MODULE__{body: %{imei: imei}, finish: true, respond: resp, log: log}

      e ->
        imei = Mvps.Packages.Parcer.check_imei(msg)
        log = %{
          log_type: "error",
          state: "authenticated",
          version: "20220314",
          message: inspect(%{auth: msg, error: e}, limit: :infinity)
        }

        resp = Mvps.Helpers.prepare_message(:mapify, "could not be connected due error in auth")
        %__MODULE__{body: %{imei: imei}, finish: true, respond: resp, log: log}
    end
  end

  def body(msg), do: Mvps.Packages.Parcer.parce_pack(msg)
end
