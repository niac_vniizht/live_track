defmodule Mvps.Packages.Parcer do
  @moduledoc """
  Предоставляет единый протокол для парсинга пакета данных
  Есть 3 варианта валидных пакетов - идентификатор первый байт -
  * 161: авторизационный пакет
  * 162: пакет данныхie
  * 163: завершающий пакет

  В пакете данных структуры передаются по блокам согласно идентификатору
  """

  def check_imei(<<161, _rest::binary>> = auth) do
    reverse = fn binlist ->
      binlist |> :binary.bin_to_list() |> Enum.reverse() |> :binary.list_to_bin()
    end

    <<161, _num::16, imei_raw::binary-size(8), _str::binary>> = auth

    <<imei::64>> = reverse.(imei_raw)
    imei
  rescue
    _e -> nil
  end

  def check_imei(_) do
    nil
  end

  def parce_pack(<<161, _rest::binary>> = pack) do
    <<end_sign::binary-size(1), key::binary-size(40), imei::64, num::16, pream::8>> =
      reverse(pack)

    %{pream: pream, num: num, imei: imei, key: reverse(key), end_sign: end_sign}
  rescue
    _e ->
      %{error: "Expected auth string but got oversized string"}
  end

  def parce_pack(<<163, _rest::binary>> = pack) do
    <<end_sign::binary-size(1), sign::8, pream::8>> = reverse(pack)
    %{pream: pream, sign: sign, end_sign: end_sign}
  end

  def parce_pack(<<162, _rest::binary>> = pack) do
    <<header::binary-size(7), rest::binary>> = pack

    # заголовок
    <<leng::16, num::32, pream::8>> = reverse(header)
    # тело
    bin_list = :binary.bin_to_list(rest)

    blocks =
      bin_list
      |> Enum.reduce_while(%{rest: rest, blocks: []}, fn _x, %{rest: rest, blocks: blocks} ->
        cond do
          rest == "\r" || rest == <<15>> || rest == "" || rest == <<13, 163, 226, 13>> ->
            {:halt, blocks}

          String.starts_with?(rest, <<13, 162>>) ->
            <<_header::binary-size(8), rest::binary>> = rest
            {:cont, %{rest: rest, blocks: blocks}}

          true ->
            [rest, pack] = calculate(rest)
            {:cont, %{rest: rest, blocks: [pack | blocks]}}
        end
      end)
      |> Enum.group_by(&"#{&1.type}_block")
      |> Enum.map(fn {k, v} -> {String.to_atom(k), Enum.map(v, &Map.delete(&1, :type))} end)
      |> Enum.into(%{})

    ~w(pream num leng blocks)a
    |> Enum.zip([pream, num, leng, blocks])
    |> Enum.into(%{})
  rescue
    _e ->
      LiveTrack.Logs.do_log(%{
        log_type: "error",
        state: "error",
        version: "20220314",
        message:
          inspect(%{error: "packages rejected due error in package structure", pack: pack},
            limit: :infinity
          )
      },
      "error",
      "mvps")

      nil
  end

  defp to_pack(3, str) do
    <<temp::8, tamb::8, reserved::8, ident::8, time::32>> = reverse(str)

    %{
      data: :binary.bin_to_list(str),
      reserved: reserved,
      pack_dt: get_unix_dt(time),
      ident: ident,
      tamb: tamb,
      temp: uint_to_int(temp)
    }
  end

  defp to_pack(10, str) do
    <<hdop::16, satellite::16, speed::16, course::16, height::32, long_h::16, long_l::32,
      lat_h::16, lat_l::32, time::32>> = reverse(str)

    lat = to_coord(lat_h, lat_l)
    lon = to_coord(long_h, long_l)

    %{
      data: :binary.bin_to_list(str),
      pack_dt: get_unix_dt(time),
      satellite: satellite,
      hdop: hdop / 100,
      speed: speed / 10,
      course: course / 10,
      height: height / 100,
      long_h: long_h,
      long_l: long_l,
      lat_h: lat_h,
      lat_l: lat_l,
      lat: lat,
      lon: lon,
      geom: try_geom(lon, lat)
    }
  end

  defp to_pack(5, str) do
    <<param_4::8, param_3::8, param_2::8, param_1::8, code::8, group::8, time::32>> = reverse(str)

    %{
      data: :binary.bin_to_list(str),
      pack_dt: get_unix_dt(time),
      group: group,
      code: code,
      param_4: param_4,
      param_3: param_3,
      param_2: param_2,
      param_1: param_1
    }
  end

  defp to_pack(9, str) do
    <<door_right_res::1, door_right_state::1, door_right_condition::1, door_right_validity::1,
      door_left_res::1, door_left_state::1, door_left_condition::1, door_left_validity::1,
      tamb::8, time::32>> = reverse(str)

    %{
      data: :binary.bin_to_list(str),
      pack_dt: get_unix_dt(time),
      tamb: tamb,
      door_right_reserved: door_right_res,
      door_right_state: door_right_state,
      door_right_condition: door_right_condition,
      door_right_validity: door_right_validity,
      door_left_reserved: door_left_res,
      door_left_state: door_left_state,
      door_left_condition: door_left_condition,
      door_left_validity: door_left_validity,
      door_right_result:
        "#{is_door_valid(door_right_validity)}; #{is_door_healthy(door_right_condition)}; #{door_state(door_right_state)}",
      door_left_result:
        "#{is_door_valid(door_left_validity)}; #{is_door_healthy(door_left_condition)}; #{door_state(door_left_state)}"
    }
  end

  defp to_pack(8, str) do
    <<count::32, no_val::8, bad_signal::8, norm_signal::8, good_signal::8, great_signal::8,
      ppru_receiver::8, ppru_sender::8, time::32>> = reverse(str)

    %{
      data: :binary.bin_to_list(str),
      pack_dt: get_unix_dt(time),
      count: count,
      no_val: no_val,
      bad_signal: bad_signal,
      norm_signal: norm_signal,
      good_signal: good_signal,
      great_signal: great_signal,
      ppru_receiver: ppru_receiver,
      ppru_sender: ppru_sender
    }
  end

  defp to_pack(_3, str) do
    %{error: "неизвестная группа", pack: str}
  end

  defp chunk_size(3), do: 8
  defp chunk_size(10), do: 28
  defp chunk_size(5), do: 10
  defp chunk_size(9), do: 6
  defp chunk_size(8), do: 15
  defp chunk_size(bn), do: %{error: "неизвестная группа", pack: bn}

  defp to_pack_name(3), do: "temperature"
  defp to_pack_name(10), do: "geo_data"
  defp to_pack_name(5), do: "telemetry"
  defp to_pack_name(9), do: "doors"
  defp to_pack_name(8), do: "rssi"
  defp to_pack_name(bn), do: %{error: "неизвестная группа", pack: bn}

  defp door_state(1), do: "дверь открыта"
  defp door_state(0), do: "дверь закрыта"

  defp is_door_healthy(1), do: "исправно"
  defp is_door_healthy(0), do: "не исправно"

  defp is_door_valid(1), do: "валидные данные"
  defp is_door_valid(0), do: "недостоверные данные"

  defp calculate(rest) do
    chunk_bits = fn string, size ->
      for <<chunk::size(size) <- string>>, do: <<chunk::size(size)>>
    end

    <<type::8, count::8, rest::binary>> = rest
    size = chunk_size(type)
    bytes = count * size
    <<block_data::binary-size(bytes), rest::binary>> = rest

    data_pack =
      block_data
      |> chunk_bits.(size * 8)
      |> Enum.map(&to_pack(type, &1))

    [rest, %{type: to_pack_name(type), count: count, pack: data_pack}]
  end

  def reverse(binlist) do
    binlist |> :binary.bin_to_list() |> Enum.reverse() |> :binary.list_to_bin()
  end

  def to_coord(high, low) do
    grad = div(high, 100)
    min_g = rem(high, 100)
    min = min_g + low / :math.pow(10, 6)
    Float.round(grad + min / 60, 6)
  rescue
    _e ->
      nil
  end

  def uint_to_int(num) when num <= 127, do: num
  def uint_to_int(num) when num > 127, do: num - 256

  def get_unix_dt(time) do
    dt = DateTime.from_unix!(time)
    if dt.year < 2022 do
      DateTime.utc_now()
    else
      dt
    end
  rescue
    _e -> DateTime.utc_now()
  end

  @spec try_geom(float(), float()) :: nil | binary
  def try_geom(lon, lat) do
    {:ok, point} = Geo.WKT.decode("SRID=4326;POINT(#{lon} #{lat})")
    point
    # |> Geo.WKT.encode!()
  rescue
    _e ->
      nil
  end
end
