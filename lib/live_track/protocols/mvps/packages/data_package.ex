defmodule Mvps.Packages.DataPackage do
  alias Mvps.Packages

  defstruct ptype: nil,
            body: nil,
            version: nil,
            respond: nil,
            log: nil,
            finish: false

  def new(msg, %{prot_version: prot_version}) do
    %__MODULE__{version: prot_version, body: body(msg)}
    |> Packages.put_log()
  end

  def body(msg), do: Mvps.Packages.Parcer.parce_pack(msg)
end
