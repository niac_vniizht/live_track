defmodule LiveTrack.Structures.ElcomBuffer do
  alias Elcom.Packages.{Login, DataPackage}
  alias LiveTrack.Packages.Helpers
  @behaviour LiveTrack.BufferBehaviour

  defstruct version: nil,
            type: __MODULE__,
            messages: [],
            opts: nil,
            auth: nil,
            log: nil,
            respond: nil,
            finish: false

  def pack_types do
    [
      L: %{
        type: "Login",
        structure: Login,
        respond: ~s|"#AL#\#{status}\r\n"|
      },
      SD: %{
        type: "Short Data",
        structure: DataPackage,
        fields: ~w(date time lat1 lat2 lon1 lon2 speed course alt sats)a,
        respond: ~s|"#ASD#\#{status}\r\n"|
      },
      D: %{
        type: "Extended Data",
        structure: DataPackage,
        fields:
          ~w(date time lat1 lat2 lon1 lon2 speed course alt sats hdop inputs outputs adc lbutton unzip_params)a,
        respond: ~s|"#AD#\#{status}\r\n"|
      },
      B: %{
        type: "Black Box",
        structure: BlackBox,
        respond: ~s|"#AB#\#{status}\r\n"|
      },
      P: %{
        type: "P",
        structure: Ping,
        respond: ~s|"#AP#\r\n"|
      }
      # US
      # UC
      # M
      # AM
      # QI
      # I
      # AI
      # QT
      # IT
      # AIT
      # T
      # AT
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: true, validate: false], opts)
    {type, parsed_message} = read_pack(line)

    protocol_version = hd(parsed_message)

    case read_message(parsed_message, to_number(protocol_version), type) do
      {:ok, %{finish: finish} = parsed_message} ->
        res =
          %__MODULE__{
            version: protocol_version,
            auth: parsed_message,
            messages: [parsed_message],
            finish: finish,
            opts: opts
          }
          |> Helpers.put_log(parsed_message, protocol_version)
          |> Helpers.put_respond(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    _e ->
      {:error, "ошибка при создании буфера"}
  end

  def add(%__MODULE__{version: protocol_version, messages: _messages} = mod, line) do
    {type, parsed_message} = read_pack(line)

    case read_message(parsed_message, to_number(protocol_version), type) do
      {:ok, %{is_end: true, buffer: buffer}} ->
        %__MODULE__{buffer | finish: true}

      {:ok, parsed_message} ->
        # IO.inspect(parsed_message)
        # %__MODULE__{mod | messages: [parsed_message | messages]}
        mod
        |> Helpers.put_log(parsed_message, protocol_version)
        |> Helpers.put_respond(parsed_message)

      _msg ->
        %__MODULE__{mod | finish: true}
    end
  end

  def read_pack(line) do
    ["", type, raw_message | _] =
      line |> String.replace("\r", "") |> String.replace("\n", "") |> String.split("#")

    type = String.to_atom(type)

    parsed_message =
      if type in [:B] do
        raw_message
      else
        raw_message
        |> String.replace("\r\n", "")
        |> String.split(";")
      end

    {type, parsed_message}
  end

  def read_message(msg, protocol_version, type) do
    valid = is_valid_type(type)

    cond do
      !valid ->
        {:error, msg}

      is_number(protocol_version) ->
        %{structure: struct} = options = Keyword.get(pack_types(), type)
        parsed_message = apply(struct, :new, [msg, options])

        {:ok, parsed_message}

      true ->
        {:error, msg}
    end
  end

  def send_data(_) do
    :ok
  end

  def finalize(_, socket) do
    :gen_tcp.close(socket)
  end

  def is_valid(line) do
    ["", type, _raw_message] = String.split(line, "#")

    is_valid_type(type)
  rescue
    _e -> false
  end

  def is_valid_login(line) do
    ["", type, _raw_message | _] = String.split(line, "#")

    type == "L"
  rescue
    _e -> false
  end

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())

  defp to_number(num_str), do: to_float(num_str) || to_int(num_str) || :error

  defp to_float(num_str) do
    String.to_float(num_str)
  rescue
    _e -> nil
  end

  defp to_int(num_str) do
    String.to_integer(num_str)
  rescue
    _e -> nil
  end
end
