defmodule Comm.Packages.DataPackage do
  alias Comm.Packages

  defstruct body: nil,
            respond: nil,
            log: nil,
            line: nil

  def new(msg, %{socket: socket, session_id: session_id}) do
    body = %{message: msg}
    case Poison.decode(msg) do
      {:error, _reason} -> {:ok, session_id}
      {:ok, command} -> LiveTrack.Handler.handle(command, %{socket: socket, session_id: session_id, conn_type: :tcp})
    end

    %__MODULE__{body: body, line: msg}
    |> Packages.put_log()
  end
end
