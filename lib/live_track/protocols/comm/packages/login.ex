defmodule Comm.Packages.Login do
  alias Comm.Packages

  defstruct body: nil,
            log: nil

  def new(access_token, _) do
    %__MODULE__{body: body(access_token)}
    |> Packages.put_log()
  end

  def body(access_token) do
    case LiveTrack.Client.validate_access_token(access_token) do
      {:ok, session} -> %{imei: 800000000000000, auth_params: %{session: session}}
      {:error, error} -> error
    end
  end
end
