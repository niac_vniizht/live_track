defmodule LiveTrack.Structures.CommBuffer do
  alias Comm.Packages.{Login, DataPackage}
  alias LiveTrack.Packages.Helpers
  @behaviour LiveTrack.BufferBehaviour

  defstruct version: nil,
            type: __MODULE__,
            messages: [],
            data_strings: [],
            opts: nil,
            auth: nil,
            session: nil,
            log: nil,
            respond: nil,
            dir: "Comm",
            finish: false

  def pack_types do
    [
      L: %{
        type: "Login",
        structure: Login
      },
      P: %{
        type: "Data Pack",
        structure: DataPackage
      }
    ]
  end

  def new(line, opts) do
    {type, parsed_message} = read_pack(line)

    case read_message(parsed_message, type, Enum.into(opts, %{})) do
      {:ok, %{body: %{imei: _, auth_params: %{session: session}}} = auth} ->
        res =
          %__MODULE__{
            version: 1,
            auth: auth,
            opts: Keyword.merge([log: true, respond: true, validate: false, session_id: session], opts)
          }
          |> Helpers.put_log(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    e ->
      IO.inspect(e)
      {:error, "ошибка при создании буфера"}
  end

  def add(%__MODULE__{messages: messages, auth: _auth, opts: opts, data_strings: data_strings} = mod, line) do
    {type, parsed_message} = read_pack(line)

    case read_message(parsed_message, type, Enum.into(opts, %{})) do
      {:ok, parsed_message} ->
        %__MODULE__{mod | messages: messages ++ [parsed_message], data_strings: data_strings ++ [line]}
        |> Helpers.put_log(parsed_message)

      _msg ->
        %__MODULE__{mod | finish: true}
    end
  end

  def read_pack(line) do
    line
    |> String.trim("\n")
    |> String.trim("\r")
    |> case do
      "niac:" <> access_token -> {:L, access_token}
      _ -> {:P, line}
    end
  end

  def read_message(msg, type, opts) do
    if is_valid_type(type) do
      %{structure: struct} = Keyword.get(pack_types(), type)
      parsed_message = apply(struct, :new, [msg, opts])
      {:ok, parsed_message}
    else
      {:error, msg}
    end
  end

  def send_data(_) do
    []
  end

  def finalize(%__MODULE__{messages: [], opts: opts}, socket) do
    LiveTrack.Client.exit_rooms(Keyword.get(opts, :session_id))
    :gen_tcp.close(socket)
  end

  def finalize(%__MODULE__{messages: messages,opts: opts} = buffer, socket) do
    LiveTrack.Client.exit_rooms(Keyword.get(opts, :session_id))
    :gen_tcp.close(socket)

    log = %{
      log_type: "warn",
      state: "has_messages",
      message: inspect(messages, limit: :infinity)
    }

    prot_dir = LiveTrack.Helpers.Utils.get_dir(buffer)

    LiveTrack.Logs.do_log(log, "log", prot_dir)
  end

  def is_valid_login(line) do
    with {:L, access_token} <- read_pack(line),
      {:ok, _session} <- LiveTrack.Client.validate_access_token(access_token) do
      true
    else
      _ -> false
    end
  rescue
    _e ->
      false
  end

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())
end
