defmodule Scout2.Header do
  import Scout2.Macros, only: [parse_field: 3, make_reduce_func: 0, make_unite: 1]

  def read(%{package: _} = pack), do: reduce(~w(frame_size packs_count)a, pack) |> unite(pack)

  make_unite(:header)
  make_reduce_func()
  parse_field(:frame_size, 4, :hex_int)
  parse_field(:packs_count, 4, :hex_int)
end
