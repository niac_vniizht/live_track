defmodule Scout2.Crc do
  import Scout2.Macros, only: [parse_field: 3, make_reduce_func: 0, make_unite: 0]

  def read(%{package: _} = pack), do: reduce(~w(crc8)a, pack) |> unite(pack)

  make_unite()
  make_reduce_func()
  parse_field(~w(crc8)a, 2, :hex_int)
end
