defmodule Scout2.Sensor do
  import Scout2.Macros, only: [parse_field: 3, make_reduce_func: 0, make_unite: 1]

  def create_sensors(pack_size, used_size, package, res \\ [])

  def create_sensors(pack_size, used_size, _package, res) when pack_size <= used_size do
    Enum.reverse(res)
  end

  def create_sensors(pack_size, used_size, package, res) do
    sensor =
      %{used_bytes: used_bytes, package: package} =
      ~w(port sensor_num sensor_type data_size data)a
      |> reduce(%{package: package, used_bytes: ""})

    create_sensors(pack_size, used_size + round(String.length(used_bytes) / 2), package, [
      sensor | res
    ])
  end

  def read(%{package: package, pack_size: pack_size, used_bytes: used_bytes} = pack) do
    create_sensors(pack_size, String.length(used_bytes) / 2, package)
    |> unite(pack)
  end

  make_unite(:sensors)
  make_reduce_func()
  parse_field(~w(port sensor_num sensor_type data_size)a, 2, :hex_int)

  parse_field(:data, :data_size, :sensor_data)
end
