defmodule Scout2.Macros do
  use Bitwise

  defmacro parse_field(field_name, binary_size, decode_type)
           when is_atom(binary_size) and is_atom(field_name) do
    quote do
      def do_parse(unquote(field_name), %{package: package, used_bytes: used_bytes} = acc) do
        size = acc[unquote(binary_size)] * 2
        <<x::binary-size(size), package::binary>> = package

        %{
          package: package,
          used_bytes: used_bytes <> x,
          "#{unquote(field_name)}": Scout2.Macros.decode(x, unquote(decode_type), acc)
        }
      end
    end
  end

  defmacro parse_field(field_names, binary_size, decode_type) when is_atom(binary_size) do
    quote do
      def do_parse(fname, %{package: package, used_bytes: used_bytes} = acc)
          when fname in unquote(field_names) do
        size = acc[unquote(binary_size)] * 2
        <<x::binary-size(size), package::binary>> = package

        %{
          package: package,
          used_bytes: used_bytes <> x,
          "#{fname}": Scout2.Macros.decode(x, unquote(decode_type), acc)
        }
      end
    end
  end

  defmacro parse_field(field_name, binary_size, decode_type) when is_atom(field_name) do
    quote do
      def do_parse(unquote(field_name), %{package: package, used_bytes: used_bytes} = acc) do
        <<x::binary-size(unquote(binary_size)), package::binary>> = package

        %{
          package: package,
          used_bytes: used_bytes <> x,
          "#{unquote(field_name)}": Scout2.Macros.decode(x, unquote(decode_type), acc)
        }
      end
    end
  end

  defmacro parse_field(field_names, binary_size, decode_type) do
    quote do
      def do_parse(fname, %{package: package, used_bytes: used_bytes} = acc)
          when fname in unquote(field_names) do
        <<x::binary-size(unquote(binary_size)), package::binary>> = package

        %{
          package: package,
          used_bytes: used_bytes <> x,
          "#{fname}": Scout2.Macros.decode(x, unquote(decode_type), acc)
        }
      end
    end
  end

  defmacro make_reduce_func() do
    quote do
      def reduce(param_list, init_state) do
        Enum.reduce_while(param_list, init_state, fn iteration_name, acc ->
          res = do_parse(iteration_name, acc)
          {:cont, Map.merge(acc, res)}
          # case do_parse(iteration_name, acc) do
          #   %{} = res -> {:cont, Map.merge(acc, res)}
          #   other -> {:halt, other}
          # end
        end)
      end
    end
  end

  defmacro make_unite(unit_name) do
    quote do
      def unite(
            %{package: line, used_bytes: parces_used_bytes} = parced,
            %{package: package, used_bytes: used_bytes} = pack
          ) do
        pack
        |> Map.put(:package, line)
        |> Map.put(:used_bytes, used_bytes <> parces_used_bytes)
        |> Map.put(unquote(unit_name), parced)
      end

      def unite(parced_list, %{package: package, used_bytes: used_bytes} = pack)
          when is_list(parced_list) do
        %{package: line} = Enum.reverse(parced_list) |> hd
        parces_used_bytes = Enum.reduce(parced_list, "", &(&2 <> &1.used_bytes))

        pack
        |> Map.put(:package, line)
        |> Map.put(:used_bytes, used_bytes <> parces_used_bytes)
        |> Map.put(unquote(unit_name), parced_list)
      end
    end
  end

  defmacro make_unite() do
    quote do
      def unite(
            %{package: line, used_bytes: parces_used_bytes} = parced,
            %{package: package, used_bytes: used_bytes} = pack
          ) do
        pack
        |> Map.merge(parced)
        |> Map.put(:package, line)
        |> Map.put(:used_bytes, used_bytes <> parces_used_bytes)
      end
    end
  end

  def decode(str, :hex_int, _), do: reverse(str, :string) |> String.to_integer(16)

  def decode(str, :id, _), do: str |> :binary.decode_hex()

  def decode(str, :hex_dt, _) do
    ns = reverse(str, :string) |> String.to_integer(16) |> Kernel.*(100)
    NaiveDateTime.add(~N[0001-01-01 00:00:00], ns, :nanosecond)
  end

  def decode(str, :hex_float, acc) do
    int_val = decode(str, :hex_int, acc)
    <<val::float-size(32)>> = <<int_val::integer-size(32)>>
    Float.round(val, 6)
  end

  def decode(_, {:status_bit, status_field, bit_num}, acc),
    do: acc[status_field] >>> bit_num &&& 1

  def decode(str, :hex_discrete, _),
    do: reverse(str, :string) |> String.to_integer(16) |> Bitwise.&&&(1)

  def decode(str, :sensor_data, %{sensor_type: data_type} = acc) do
    case data_type do
      1 -> decode(str, :hex_float, acc)
      2 -> decode(str, :hex_int, acc)
      3 -> decode(str, :hex_discrete, acc)
      _ -> str
    end
  end

  def decode(val, _, _), do: val

  def reverse(binlist, :string),
    do:
      binlist
      |> :binary.bin_to_list()
      |> Enum.chunk_every(2)
      |> Enum.reverse()
      |> List.flatten()
      |> :binary.list_to_bin()
end
