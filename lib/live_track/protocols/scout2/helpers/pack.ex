defmodule Scout2.Pack do
  import Scout2.Macros, only: [parse_field: 3, make_reduce_func: 0, make_unite: 1]

  def create_packs(packs_count, package, res \\ [])

  def create_packs(packs_count, _package, res) when packs_count <= length(res) do
    Enum.reverse(res)
  end

  def create_packs(packs_count, package, res) do
    sensor =
      %{package: package} =
      ~w(
        pack_size pack_type device_type serial_len serial device_dt lon lat speed course satellites_count
        status0 fix odometer status1 main_power movement ignition_on status1_valid
      )a
      |> reduce(%{package: package, used_bytes: ""})
      |> Scout2.Sensor.read()

    create_packs(packs_count, package, [sensor | res])
  end

  def read(%{package: package, header: %{packs_count: packs_count}} = pack) do
    create_packs(packs_count, package)
    |> unite(pack)
  end

  make_unite(:packs)
  make_reduce_func()
  parse_field(~w(pack_size pack_type)a, 4, :hex_int)
  parse_field(~w(device_type)a, 8, :hex_int)
  parse_field(~w(serial_len)a, 2, :hex_int)
  parse_field(~w(serial)a, :serial_len, :id)
  parse_field(~w(device_dt)a, 16, :hex_dt)
  parse_field(~w(lon lat speed)a, 8, :hex_float)
  parse_field(~w(course)a, 4, :hex_int)
  parse_field(~w(satellites_count status0)a, 2, :hex_int)
  parse_field(:fix, 0, {:status_bit, :status0, 0})
  parse_field(:odometer, 8, :hex_int)
  parse_field(:status1, 4, :hex_int)
  parse_field(:main_power, 0, {:status_bit, :status1, 0})
  parse_field(:movement, 0, {:status_bit, :status1, 1})
  parse_field(:ignition_on, 0, {:status_bit, :status1, 2})
  parse_field(:status1_valid, 0, {:status_bit, :status1, 15})
end
