defmodule LiveTrack.Structures.Scout2Buffer do
  use Bitwise
  # alias Scout2.Packages
  alias Scout2.Packages.{DataPackage}
  alias Helpers.SqlSupQuery
  alias LiveTrack.Packages.Helpers

  defstruct version: nil,
            type: __MODULE__,
            data_strings: [],
            messages: [],
            opts: nil,
            auth: nil,
            log: nil,
            respond: nil,
            db_key: :auth167_live_track,
            db_table: "scout",
            dir: "scout",
            finish: false

  def pack_types do
    [
      P: %{
        type: "Package",
        structure: DataPackage
      }
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: true, validate: false], opts)
    {type, r_pack} = read_pack(line)

    protocol_version = 2

    case read_message(r_pack, 1, type) do
      {:ok, %DataPackage{body: %{packs: packs}, auth: auth} = parsed_message} ->
        res =
          %__MODULE__{
            version: protocol_version,
            auth: auth,
            messages: transform_for_insert(packs),
            data_strings: [r_pack],
            opts: opts
          }
          |> Helpers.put_log(parsed_message, protocol_version)
          |> Helpers.put_respond(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    e ->
      IO.inspect(e)
      {:error, "ошибка при создании буфера"}
  end

  def add(
        %__MODULE__{version: protocol_version, messages: messages, data_strings: data_strings} =
          mod,
        line
      ) do
    {type, r_pack} = read_pack(line)

    case read_message(r_pack, 1, type) do
      {:ok, %DataPackage{body: %{packs: packs}} = parsed_message} ->
        %__MODULE__{
          mod
          | messages: messages ++ transform_for_insert(packs),
            data_strings: data_strings ++ [r_pack]
        }
        |> Helpers.put_log(parsed_message, protocol_version)
        |> Helpers.put_respond(parsed_message)
    end
  end

  def read_pack(line) do
    type = :P
    parsed_line = :binary.encode_hex(line)
    {type, parsed_line}
  end

  def read_message(msg, protocol_version, type) do
    valid = is_valid_type(type)

    cond do
      !valid ->
        {:error, msg}

      is_number(protocol_version) ->
        %{structure: struct} = Keyword.get(pack_types(), type)
        options = %{type: type}
        parsed_message = apply(struct, :new, [msg, options])

        {:ok, parsed_message}

      true ->
        {:error, msg}
    end
  end

  def transform_for_insert(parsed_packs) when is_map(parsed_packs),
    do: transform_for_insert([parsed_packs])

  def transform_for_insert(parsed_packs) when is_list(parsed_packs) do
    Enum.map(parsed_packs, fn pack ->
      pack
      |> Map.take([:device_type, :lon, :lat, :speed, :odometer, :course])
      |> Map.put(:pack_dt, Map.get(pack, :device_dt))
      |> Map.put(:server_dt, NaiveDateTime.utc_now())
      |> Map.put(:data, inspect(pack, limit: :infinity))
      |> Map.put(:imei, Map.get(pack, :serial))
    end)
  end

  def send_data(%__MODULE__{messages: messages, db_table: db_table, db_key: db_key, auth: %{body: auth_body}}) do
    messages = List.flatten(messages)
    messages |> SqlSupQuery.insert(db_table, db_key)

    LiveTrack.Writters.Helpers.update_or_insert_action(:scout, length(messages), auth_body, db_key)
    LiveTrack.Writters.Helpers.update_or_insert_last_info(auth_body, db_key)
  end

  def finalize(%__MODULE__{messages: []}, socket) do
    :gen_tcp.close(socket)
  end

  def finalize(%__MODULE__{messages: messages} = buffer, socket) do
    :gen_tcp.close(socket)

    log = %{
      log_type: "warn",
      state: "has_messages",
      message: inspect(messages, limit: :infinity)
    }

    prot_dir = LiveTrack.Helpers.Utils.get_dir(buffer)

    LiveTrack.Logs.do_log(log, "log", prot_dir)

    try do
      LiveTrack.Logs.do_log(log, buffer.auth.body.imei, prot_dir)
    rescue
      _ -> :ok
    end
  end

  def is_valid_login(_line) do
    true
    # line = line |> String.replace("\r", "") |> String.replace("\n", "")
    # case line do
    #   <<_::binary-size(8)>> -> true
    #   _ -> false
    # end
  end

  # def is_valid_login(<<_::binary-size(8)>>), do: true
  # def is_valid_login(_), do: false

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())

  # def is_valid(line) do
  #   ["", type, _raw_message] = String.split(line, "#")

  #   is_valid_type(type)
  # rescue
  #   _e -> false
  # end
end
