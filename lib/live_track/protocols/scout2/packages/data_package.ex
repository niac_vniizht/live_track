defmodule Scout2.Packages.DataPackage do
  use Bitwise
  alias Scout2.Packages

  defstruct ptype: nil,
            body: nil,
            auth: nil,
            respond: nil,
            log: nil,
            line: nil

  def new(msg, %{type: type}) do
    body = body(msg)
    imei = body.packs |> hd() |> Map.get(:serial)

    %__MODULE__{ptype: type, body: body, auth: %{body: %{imei: "#{imei}"}}, line: msg}
    |> Packages.put_respond()
    |> Packages.put_log()
  end

  def body(line) do
    %{header: header, packs: packs} = parsed = read_pack(line)
    header = delete_extra(header)

    packs =
      packs
      |> Enum.map(fn pack ->
        sensors = Map.get(pack, :sensors) |> Enum.map(&delete_extra(&1))

        pack =
          pack
          |> delete_extra()
          |> Map.put(:sensors, sensors)

        Map.put(pack, :data, inspect(pack, limit: :infinity))
      end)

    parsed
    |> Map.put(:header, header)
    |> Map.put(:packs, packs)
    |> delete_extra()
  end

  def delete_extra(map) do
    map |> Map.delete(:used_bytes) |> Map.delete(:package)
  end

  def read_pack(line) do
    %{package: line, used_bytes: ""}
    |> Scout2.Header.read()
    |> Scout2.Pack.read()
    |> Scout2.Crc.read()
  end
end
