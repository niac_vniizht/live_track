defmodule Scout.Packages.DataPackage do
  use Bitwise
  alias Scout.Packages

  defstruct ptype: nil,
            body: nil,
            respond: nil,
            log: nil,
            line: nil,
            rem_messages: nil,
            packs_counter: nil,
            finish: nil

  def new(msg, %{type: type, packs_counter: packs_counter}) do
    {body, rem_messages, packs_counter} = create_packs_maps(packs_counter, msg, [])

    %__MODULE__{
      ptype: type,
      body: body,
      rem_messages: rem_messages,
      packs_counter: packs_counter,
      line: msg
    }
    |> Packages.put_respond()
    |> Packages.put_log()

    # |> Packages.finalize()
  end

  def create_packs_maps(count, [], res), do: {res, [], count}

  def create_packs_maps(0, list, res), do: {res, list, 0}

  def create_packs_maps(count, list, res) do
    fields = [
      :serial_len,
      :serial,
      :protocol_id,
      :datetime,
      :lon,
      :lat,
      :speed,
      :course,
      :dig_io,
      :adc0,
      :adc1,
      :stat0,
      :main_power,
      :stat1,
      :satellites_num,
      :gps_fix,
      :data_size,
      :data_length,
      :data
    ]

    {m, list} =
      Enum.reduce(fields, {%{}, list}, fn
        field, {map, _} = acc ->
          if field in fields do
            {val, curr_list} = read_field(field, acc)
            {Map.put(map, field, val), curr_list}
          else
            acc
          end

        _, acc ->
          acc
      end)

    create_packs_maps(count - 1, list, [m | res])
  end

  def read_field(_, {_, []}), do: {nil, []}

  def read_field(:serial_len, {_, [h | curr_list]}) do
    serial_len = h |> Integer.parse(16) |> elem(0)
    {serial_len, curr_list}
  end

  def read_field(:serial, {%{serial_len: serial_len}, curr_list}) do
    serial =
      Enum.slice(curr_list, 0..(serial_len - 1))
      |> Enum.map(&:binary.decode_hex(&1))
      |> Enum.reduce("", fn el, acc -> acc <> el end)
      |> Integer.parse()
      |> elem(0)
      |> to_string()

    {serial, Enum.slice(curr_list, serial_len..-1)}
  end

  def read_field(:protocol_id, {_, curr_list}) do
    protocol_id =
      Enum.slice(curr_list, 0..3)
      |> Enum.reverse()
      |> Enum.join()
      |> Integer.parse(16)
      |> elem(0)

    {protocol_id, Enum.slice(curr_list, 4..-1)}
  end

  def read_field(:datetime, {_, curr_list}) do
    nanosec =
      curr_list
      |> Enum.slice(0..7)
      |> Enum.reverse()
      |> Enum.join()
      |> Integer.parse(16)
      |> elem(0)
      |> Kernel.*(100)

    datetime = DateTime.add(~U[0001-01-01 00:00:00Z], nanosec, :nanosecond)

    {datetime, Enum.slice(curr_list, 8..-1)}
  end

  def read_field(field, {_, curr_list}) when field in [:lat, :lon, :speed] do
    int_val =
      Enum.slice(curr_list, 0..3)
      |> Enum.reverse()
      |> Enum.join()
      |> Integer.parse(16)
      |> elem(0)

    <<val::float-size(32)>> = <<int_val::integer-size(32)>>
    val = Float.round(val, 6)

    {val, Enum.slice(curr_list, 4..-1)}
  end

  def read_field(field, {_, curr_list}) when field in [:course, :adc0, :adc1] do
    val =
      Enum.slice(curr_list, 0..1)
      |> Enum.reverse()
      |> Enum.join()
      |> Integer.parse(16)
      |> elem(0)

    {val, Enum.slice(curr_list, 2..-1)}
  end

  def read_field(field, {_, curr_list}) when field in [:dig_io, :stat0, :stat1] do
    val =
      Enum.slice(curr_list, 0..1)
      |> Enum.reverse()
      |> Enum.join()
      |> Integer.parse(16)
      |> elem(0)

    {val, Enum.slice(curr_list, 2..-1)}
  end

  def read_field(field, {%{stat0: stat0}, curr_list}) when field in [:main_power] do
    val =
      stat0
      |> Integer.to_string(2)
      |> String.at(2)
      |> Kernel.==("1")

    {val, curr_list}
  end

  def read_field(field, {%{stat1: stat1}, curr_list}) when field in [:satellites_num] do
    val =
      stat1
      |> Integer.to_string(2)
      |> String.slice(-5..-1)
      |> :erlang.binary_to_integer(2)

    {val, curr_list}
  end

  def read_field(field, {%{stat1: stat1}, curr_list}) when field in [:gps_fix] do
    val =
      stat1
      |> Integer.to_string(2)
      |> String.slice(-8..-7)
      |> :erlang.binary_to_integer(2)
      |> Kernel.>(0)

    {val, curr_list}
  end

  def read_field(field, {_, curr_list}) when field in [:data_size] do
    lo = Enum.at(curr_list, 0) |> :erlang.binary_to_integer(16)

    if lo < 128 do
      {lo, Enum.slice(curr_list, 1..-1)}
    else
      lo = lo &&& 0x7F

      val =
        Enum.at(curr_list, 1)
        |> :erlang.binary_to_integer(16)
        |> Bitwise.bsl(7)
        |> Kernel.+(lo)

      {val, Enum.slice(curr_list, 2..-1)}
    end
  end

  def read_field(field, {%{data_size: data_size}, curr_list}) when field in [:data_length] do
    new_list =
      curr_list
      |> Enum.slice(0..(data_size - 1))
      |> Enum.map(&:binary.decode_hex(&1))

    {new_list, curr_list} =
      if rem(length(new_list), 2) != 0 do
        {["0" | new_list], Enum.slice(curr_list, 1..-1)}
      else
        {new_list, Enum.slice(curr_list, 2..-1)}
      end

    val =
      new_list
      |> Enum.slice(0..1)
      |> Enum.join()
      |> :erlang.binary_to_integer(16)

    {val, curr_list}
  end

  def read_field(field, {%{data_length: data_length}, curr_list}) when field in [:data] do
    val =
      curr_list
      |> Enum.slice(0..(data_length * 2 - 1))
      |> Enum.map(&:binary.decode_hex(&1))
      |> Enum.join()
      |> Packages.chunk_bits(16)
      |> Enum.reduce([], fn
        el, [] ->
          [%{type: :erlang.binary_to_integer(el, 16)}]

        el, [%{type: type, len: len, data: data} | tail] when length(data) < len ->
          [%{type: type, len: len, data: [el | data]} | tail]

        el, [%{type: _, len: _, data: _} | _] = acc ->
          [%{type: :erlang.binary_to_integer(el, 16)} | acc]

        el, [%{type: type} | tail] ->
          [%{type: type, len: :erlang.binary_to_integer(el, 16), data: []} | tail]

        _, acc ->
          acc
      end)
      |> Enum.reverse()
      |> Enum.map(fn %{data: data} = map ->
        data = data |> Enum.reverse() |> Enum.join() |> :erlang.binary_to_integer(16)
        Map.put(map, :data, data)
      end)

    {val, Enum.slice(curr_list, (data_length * 2)..-1)}
  end
end
