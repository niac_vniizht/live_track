defmodule Scout.Packages.DataPackagesCount do
  alias Scout.Packages

  defstruct ptype: nil,
            body: nil,
            respond: nil,
            status: nil,
            log: nil,
            line: nil,
            finish: false

  def new(msg, %{type: type}) do
    packs_counter = body(msg)

    if packs_counter == 0 do
      %__MODULE__{ptype: type, body: packs_counter, line: msg}
      |> Packages.put_respond()
      |> Packages.put_log()
    else
      %__MODULE__{ptype: type, body: packs_counter, line: msg}
      |> Packages.put_log()
    end
  end

  def body(msg) do
    msg
    |> Enum.reverse()
    |> Enum.join()
    |> :erlang.binary_to_integer(16)
  end
end
