defmodule LiveTrack.Structures.WialonBuffer do
  @behaviour LiveTrack.BufferBehaviour
  alias Wialon.Packages.{Login, DataPackage, BlackBox, Ping}
  # alias LiveTrack.Packages.Helpers

  defstruct version: nil,
            type: __MODULE__,
            data_strings: [],
            messages: [],
            opts: nil,
            auth: nil,
            log: nil,
            db_key: :auth167_live_track,
            db_table: "wialon",
            respond: nil,
            dir: "wialon",
            finish: false

  def pack_types do
    [
      L: %{
        type: "Login",
        structure: Login,
        respond: ~s|"#AL#\#{status}\r\n"|
      },
      SD: %{
        type: "Short Data",
        structure: DataPackage,
        fields: ~w(date time lat1 lat2 lon1 lon2 speed course alt sats)a,
        respond: ~s|"#ASD#\#{status}\r\n"|
      },
      D: %{
        type: "Extended Data",
        structure: DataPackage,
        fields:
          ~w(date time lat1 lat2 lon1 lon2 speed course alt sats hdop inputs outputs adc lbutton unzip_params)a,
        respond: ~s|"#AD#\#{status}\r\n"|
      },
      B: %{
        type: "Black Box",
        structure: BlackBox,
        respond: ~s|"#AB#\#{status}\r\n"|
      },
      P: %{
        type: "P",
        structure: Ping,
        respond: ~s|"#AP#\r\n"|
      }
      # US
      # UC
      # M
      # AM
      # QI
      # I
      # AI
      # QT
      # IT
      # AIT
      # T
      # AT
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: true, validate: false], opts)
    {type, parsed_message} = read_pack(line)

    protocol_version = hd(parsed_message)

    case read_message(parsed_message, to_number(protocol_version), type) do
      {:ok, %{finish: finish} = parsed_message} ->
        res =
          %__MODULE__{
            version: protocol_version,
            auth: parsed_message,
            finish: finish,
            opts: opts
          }
          |> LiveTrack.Packages.Helpers.put_log(parsed_message, protocol_version)
          |> LiveTrack.Packages.Helpers.put_respond(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    e ->
      IO.inspect(e)
      {:error, "ошибка при создании буфера"}
  end

  def add(
        %__MODULE__{
          version: protocol_version,
          messages: messages,
          auth: auth,
          data_strings: data_strings
        } = mod,
        line
      ) do
    {type, parsed_message} = read_pack(line)

    case read_message(parsed_message, to_number(protocol_version), type) do
      {:ok, %{is_end: true, buffer: buffer}} ->
        %__MODULE__{buffer | finish: true}

      {:ok, %BlackBox{body: body} = parsed_message} ->
        %__MODULE__{
          mod
          | messages: messages ++ transform_for_insert(body, auth),
            data_strings: data_strings ++ [line]
        }
        |> LiveTrack.Packages.Helpers.put_log(parsed_message, protocol_version)
        |> LiveTrack.Packages.Helpers.put_respond(parsed_message)

      {:ok, parsed_message} ->
        %__MODULE__{
          mod
          | messages: messages ++ [transform_for_insert(parsed_message, auth)],
            data_strings: data_strings ++ [line]
        }
        |> LiveTrack.Packages.Helpers.put_log(parsed_message, protocol_version)
        |> LiveTrack.Packages.Helpers.put_respond(parsed_message)

      _msg ->
        %__MODULE__{mod | finish: true}
    end
  end

  def read_pack(line) do
    ["", type, raw_message | _] =
      line |> String.replace("\r", "") |> String.replace("\n", "") |> String.split("#")

    type = String.to_atom(type)

    parsed_message =
      if type in [:B] do
        raw_message
      else
        raw_message
        |> String.replace("\r\n", "")
        |> String.split(";")
      end

    {type, parsed_message}
  end

  def read_message(msg, protocol_version, type) do
    valid = is_valid_type(type)

    cond do
      !valid ->
        {:error, msg}

      is_number(protocol_version) ->
        %{structure: struct} = options = Keyword.get(pack_types(), type)
        parsed_message = apply(struct, :new, [msg, options])

        {:ok, parsed_message}

      true ->
        {:error, msg}
    end
  end

  def transform_for_insert(%{pack_dt: pack_dt, dt: dt, lon: lon, lat: lat, speed: speed, course: course, alt: alt} = pack, %{
        body: %{imei: imei}
      }) do
    pack_dt =
      case pack_dt do
        %NaiveDateTime{} -> pack_dt
        _ -> nil
      end

    %{
      pack_dt: pack_dt,
      server_dt: dt,
      lon: lon,
      lat: lat,
      speed: speed,
      course: course,
      altitude: alt,
      data: inspect(pack, limit: :infinity),
      imei: "#{imei}"
    }
  end

  def transform_for_insert(%{body: body}, auth), do: transform_for_insert(body, auth)

  def transform_for_insert(list, auth) when is_list(list),
    do: Enum.map(list, &transform_for_insert(&1, auth)) |> Enum.filter(& &1)

  def transform_for_insert(_, _), do: nil

  def send_data(%__MODULE__{messages: messages, db_table: db_table, db_key: db_key, auth: %{body: auth_body}}) do
    messages = List.flatten(messages)
    messages |> Helpers.SqlSupQuery.insert(db_table, db_key)

    LiveTrack.Writters.Helpers.update_or_insert_action(:wialon, length(messages), auth_body, db_key)
    LiveTrack.Writters.Helpers.update_or_insert_last_info(auth_body, db_key)
  end

  def finalize(%__MODULE__{messages: []}, socket) do
    :gen_tcp.close(socket)
  end

  def finalize(%__MODULE__{messages: messages} = buffer, socket) do
    :gen_tcp.close(socket)

    prot_dir = LiveTrack.Helpers.Utils.get_dir(buffer)

    log = %{
      log_type: "warn",
      state: "has_messages",
      message: inspect(messages, limit: :infinity)
    }

    LiveTrack.Logs.do_log(log, "log", prot_dir)

    try do
      LiveTrack.Logs.do_log(log, buffer.auth.body.imei, prot_dir)
    rescue
      _ -> :ok
    end
  end

  def is_valid(line) do
    ["", type, _raw_message] = String.split(line, "#")

    is_valid_type(type)
  rescue
    _e -> false
  end

  def is_valid_login(line) do
    ["", type, _raw_message | _] = String.split(line, "#")

    type == "L"
  rescue
    _e -> false
  end

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())

  defp to_number(num_str), do: to_float(num_str) || to_int(num_str) || :error

  defp to_float(num_str) do
    String.to_float(num_str)
  rescue
    _e -> nil
  end

  defp to_int(num_str) do
    String.to_integer(num_str)
  rescue
    _e -> nil
  end
end
