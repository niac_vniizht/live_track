defmodule Wialon.Packages do
  def put_respond(mod, respond) do
    {resp, _} = Code.eval_string(respond, Map.to_list(mod))
    Map.put(mod, :respond, resp)
  rescue
    _e ->
      Map.put(mod, :respond, "#AL#0\r\n")
  end

  def invalid_imei?(_imei), do: false

  def invalid_time?(%{date: date, time: time}),
    do:
      !(!is_nil(date) && String.length(date) == 6 && (!is_nil(time) && String.length(time) == 6))

  def invalid_coordintes?(%{lat: lat, lon: lon}) do
    lat == :error || lon == :error || lat >= 90 || lat <= -90 || lon >= 180 || lon <= -180
  end

  def invalid_speed?(%{speed: speed}), do: speed == :error || speed < 0
  def invalid_sattelites?(%{sats: sats}), do: sats == :error || sats < 0
  def invalid_io?(%{inputs: inputs, outputs: outputs}), do: inputs == :error || outputs == :error

  def invalid_adc?(map) do
    Enum.reduce_while(map, false, fn {k, v}, _acc ->
      if String.match?("#{k}", ~r/adc/) && v == :error do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end

  def invalid_additionals?(map) do
    Enum.reduce_while(map, false, fn {k, v}, _acc ->
      if String.match?("#{k}", ~r/adc/) && v == :error do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end

  def invalid_crc?(msg) do
    # [crc | rev] = Enum.reverse(msg)

    # rev |>
    # Enum.reverse() |>
    # Enum.reduce("", fn val, acc -> "#{acc}#{val};" end) |>
    # LiveTrack.Crc16.calc() |>
    # Integer.to_string() |>
    # Kernel.!==(crc)
    !msg
  end

  def invalid_password?(password) do
    case wialon_password() do
      list when is_list(list) -> password in list
      string when is_binary(string) -> password == string
      _ -> throw("Необходимо добавить пароль в конфигурационный файл")
    end
    |> Kernel.!()
  end

  def put_log(mod),
    do: %{mod | log: %{log_type: "info", state: "server", message: inspect(mod.body, limit: :infinity)}}

  def finalize(%{status: "1"} = mod), do: mod
  def finalize(mod), do: %{mod | finish: true}
  defp wialon_password, do: Application.get_env(:server, :wialon_password)
end

defimpl Inspect, for: Wialon.Packages.Login do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.Login{} = package, opts) do
    list =
      for attr <- [:status, :body] do
        {attr, Map.get(package, attr)}
      end

    container_doc("#Login<", list, ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end

defimpl Inspect, for: Wialon.Packages.DataPackage do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.DataPackage{ptype: ptype} = package, opts) do
    list =
      for attr <- [:status, :body] do
        {attr, Map.get(package, attr)}
      end

    container_doc("##{ptype}<", list, ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end

defimpl Inspect, for: Wialon.Packages.BlackBox do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.BlackBox{} = package, opts) do
    list =
      for attr <- [:status, :body] do
        {attr, Map.get(package, attr)}
      end

    container_doc("#BlackBox<", list, ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end

defimpl Inspect, for: Wialon.Packages.Ping do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.Ping{}, opts) do
    container_doc("#Pong<", [], ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end
