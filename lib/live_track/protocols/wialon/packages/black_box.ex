defmodule Wialon.Packages.BlackBox do
  alias Wialon.Packages

  defstruct ptype: nil,
            body: nil,
            status: nil,
            log: nil,
            respond: nil

  def new(msg, %{respond: respond}) do
    msg = String.split(msg, "|")
    package = body(msg)

    %__MODULE__{body: package, status: status(msg, package)}
    |> Packages.put_respond(respond)
    |> put_log()
  end

  def body(msg) do
    msg
    |> Enum.map(fn str ->
      msg = String.split(str, ";")
      len = length(msg)

      cond do
        len == 1 ->
          nil

        len == 10 && len == 11 ->
          Wialon.Packages.DataPackage.new(
            msg,
            Keyword.get(LiveTrack.Structures.WialonBuffer.pack_types(), :SD)
          )

        true ->
          Wialon.Packages.DataPackage.new(
            msg,
            Keyword.get(LiveTrack.Structures.WialonBuffer.pack_types(), :D)
          )
      end
    end)
    |> Enum.filter(& &1)
  end

  def status(msg, body) do
    crc = List.last(body)

    crc_calced =
      (msg -- [crc])
      |> Enum.reduce("", fn x, acc -> "#{acc}#{x}|" end)
      |> LiveTrack.Crc16.calc()
      |> Integer.to_string()

    cond do
      is_binary(crc) && crc_calced == crc -> ""
      true -> "#{length(body)}"
    end
  end

  defp put_log(mod),
    do: %__MODULE__{mod | log: %{log_type: "info", state: :black_box, message: inspect(mod, limit: :infinity)}}
end
