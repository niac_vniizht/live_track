defmodule Niac.Packages.DataPackage do
  alias Niac.Packages

  defstruct body: nil,
            respond: nil,
            log: nil,
            line: nil

  def new(msg, _) do
    body = %{message: msg}

    %__MODULE__{body: body, line: msg}
    |> Packages.put_log()
  end
end
