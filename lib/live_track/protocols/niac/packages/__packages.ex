defmodule Niac.Packages do
  def put_respond(mod) do
    Map.put(mod, :respond, :binary.decode_hex("55"))
  end

  def invalid_imei?(_imei), do: false

  def invalid_io?(%{inputs: inputs, outputs: outputs}), do: inputs == :error || outputs == :error

  def put_log(mod),
    do: %{mod | log: %{log_type: "info", state: "server", message: inspect(mod.body)}}
end
