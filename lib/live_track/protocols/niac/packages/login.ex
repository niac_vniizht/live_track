defmodule Niac.Packages.Login do
  alias Niac.Packages

  defstruct body: nil,
            log: nil

  def new(auth, _) do
    %__MODULE__{body: body(auth)}
    # |> Packages.put_log()
  end

  def body([_, imei | _] = auth), do: %{imei: imei, auth_params: auth}
end
