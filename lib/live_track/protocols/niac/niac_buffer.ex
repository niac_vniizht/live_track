defmodule LiveTrack.Structures.NiacBuffer do
  alias Niac.Packages
  alias Niac.Packages.{Login, DataPackage}
  alias LiveTrack.Packages.Helpers
  require Logger
  @behaviour LiveTrack.BufferBehaviour

  defstruct version: nil,
            type: __MODULE__,
            messages: [],
            message_part: "",
            data_strings: [],
            opts: nil,
            auth: %{},
            logs: [],
            respond: nil,
            db_key: :auth167_live_track,
            dir: "niac",
            finish: false

  def pack_types do
    [
      L: %{
        type: "Login",
        structure: Login
      },
      P: %{
        type: "Data Pack",
        structure: DataPackage
      },
      UP: %{
        type: "Unfinished Data Pack"
      }
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: true, validate: false], opts)
    {type, parsed_message} = read_login_pack(line)

    case read_message(parsed_message, type) do
      {:ok, logs} ->
        res =
          %__MODULE__{
            version: 1,
            auth: Login.new(parsed_message, opts),
            opts: opts
          }
          |> Helpers.put_log(logs)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    e ->
      Logger.warn("Error NIAC: #{inspect e}")
      {:error, "ошибка при создании буфера"}
  end

  def add(
        %__MODULE__{messages: messages, message_part: message_part, data_strings: data_strings} =
          mod,
        line
      ) do
    LiveTrack.Helpers.Utils.split_line(message_part <> line)
    |> Enum.reduce(mod, fn msg, acc ->
      {type, parsed_message} = read_pack(msg)

      case read_message(parsed_message, type) do
        {:ok, %{body: %{message_part: message_part}} = p} ->
          %__MODULE__{acc | message_part: message_part, data_strings: data_strings ++ [line]}
          |> Helpers.put_log(p)

        {:ok, %{body: %{message: parsed_message}} = p} ->
          %__MODULE__{
            acc
            | messages: messages ++ [parsed_message],
              message_part: "",
              data_strings: data_strings ++ [line]
          }
          |> Helpers.put_log(p)
        _msg ->
          %__MODULE__{mod | finish: true}
      end
    end)
  end

  def read_pack(line) do
    if String.match?(line, ~r/\n/) do
      parsed_message = line |> String.replace("\r", "") |> String.replace("\n", "")
      
      case String.split(parsed_message, ",") do
        ["2", _, _] = list -> {:L, list}
        ["2", _, _, _] = list -> {:L, list}
        ["0"] -> {:F, parsed_message}
        _ -> {:P, parsed_message}
      end
    else
      {:UP, line}
    end
  end

  def read_message(msg, type) do
    cond do
      not is_valid_type(type) ->
        {:error, msg}

      type == :UP ->
        {:ok, %{body: %{message_part: msg}, log: nil} |> Packages.put_log()}

      true ->
        %{structure: struct} = Keyword.get(pack_types(), type)
        parsed_message = apply(struct, :new, [msg, %{}])
        {:ok, parsed_message}
    end
  end

  def send_data(%__MODULE__{messages: []}) do
    []
  end

  def send_data(%__MODULE__{auth: %{body: %{auth_params: auth_params} = _auth_body}, db_key: _db_key, messages: messages}) do
    LiveTrack.Store.Supervisor.send_to_store(
      :niac_store,
      LiveTrack.Transform.Server.transform_buffer_message(
        %{auth_params: auth_params, packs: messages}, 
        "Niac",
        NaiveDateTime.add(NaiveDateTime.utc_now(), 3 * 60 * 60), 
        :zip
      )
    )
    # LiveTrack.Writters.Helpers.update_or_insert_action(:niac, length(messages), auth_body, db_key)
    # LiveTrack.Writters.Helpers.update_or_insert_last_info(auth_body, db_key)
  end

  def finalize(%__MODULE__{messages: []}, socket) do
    :gen_tcp.send(socket, "4\n")
    IO.puts("отправлен 4\\n")
    :gen_tcp.close(socket)
  end
  
  def finalize(%__MODULE__{messages: messages} = buffer, socket) do
    :gen_tcp.send(socket, "4\n")
    IO.puts("отправлен 4\\n")
    :gen_tcp.close(socket)

    log = %{
      log_type: "warn",
      state: "has_messages",
      message: inspect(messages, limit: :infinity)
    }

    prot_dir = LiveTrack.Helpers.Utils.get_dir(buffer)

    LiveTrack.Logs.do_log(log, "log", prot_dir)

    try do
      LiveTrack.Logs.do_log(log, buffer.auth.body.imei, prot_dir)
    rescue
      _ -> :ok
    end
  end

  def is_valid_login(line) do
    {type, _} = read_login_pack(line)
    type == :L
  rescue
    _e ->
      false
  end

  def read_login_pack(line) do
    parsed_message = line |> String.replace("\r", "") |> String.replace("\n", "")
    
    case String.split(parsed_message, ",") do
      ["2", _, _] = list -> {:L, list}
      ["2", _, _, _] = list -> {:L, list}
      ["0"] -> {:F, parsed_message}
      _ -> {:P, parsed_message}
    end
  end


  defp is_valid_type(type), do: type in Keyword.keys(pack_types())
end

defimpl Inspect, for: LiveTrack.Structures.NiacBuffer do
  import Inspect.Algebra

  def inspect(%{} = package, opts) do
    list =
      for attr <- [:version, :auth, :line] do
        {attr, Map.get(package, attr)}
      end

    container_doc("#NIAC_BUFFER<", list, ">", opts, fn
      {:version, version}, opts -> concat("version: ", to_doc(version, opts))
      {:auth, auth}, opts -> concat("auth: ", to_doc(Map.get(auth, :body, "..."), opts))
      {:line, line}, opts -> concat("line: ", to_doc(line, opts))
    end)
  end
end
