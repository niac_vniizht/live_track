defmodule LiveTrack.Worker do
  @moduledoc """
  Машина для стандартной приемки данных, первая строка обрабатывает параметры машины (авторизация)
  Если пакеты, передаваемые устройством, но автомат занят, то они хранятся в буфере сообщений и считываются по одному пакету
  По мере чтения данных с устройства автомат записывает данные в собственное состояние, по окончанию записывает в файл неразобранные данные и пишет в базу разобранные пакеты.

  Состояния автомата -
  :inited - инициализация и запуск автомата
  :waiting - ожидание пакета (сессия не завершена)
  :connected - завершение записи

  События (автомат самостоятельно по времени выполнения и временным ограничениям переключает собственные режимы) -
  {:connect, socket, line} - инициализация сокета (сессии) и первого пакета, который был проверен ранее
  :read_response - ожидание, чтение и проверка ответа
  {:check, line} - проверка сообщения на валидность
  {:store, next_line} - запись сообщения в буфер
  :finalize - завершение связи и выполнение записи в файл и в базу данных
  :check_state - для внешних запросов, проверка состояния, памяти, событий, которые происходят с автоматом
  """
  @behaviour :gen_statem
  require Logger
  alias LiveTrack.Helpers.Utils

  defstruct auth: nil,
            buffer: nil,
            socket: nil

  @doc false
  def start_link(name), do: start(name)

  @doc false
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent
    }
  end

  @doc false
  def start({:via, Registry, {_registry_name, auth}} = name) do
    :gen_statem.start_link(name, __MODULE__, auth, [])
  end

  @doc false
  def stop(name), do: :gen_statem.stop(name)


  @doc false
  @impl :gen_statem
  def init(auth), do: {:ok, :inited, %__MODULE__{auth: auth}}

  @impl :gen_statem
  @spec callback_mode :: :handle_event_function
  def callback_mode, do: :handle_event_function

  @impl :gen_statem
  def handle_event({:call, from}, {:run, socket, buffer}, :inited, data) do
    buffer = Utils.run_buffer(socket, buffer)

    {:next_state, :waiting, %__MODULE__{data | socket: socket, buffer: buffer},
     [{:state_timeout, 1, :read_response}, {:reply, from, {:ok, :running}}]}
  end

  def handle_event({:call, _from}, {:store, buffer}, _, %{socket: _socket} = data) do
    {:next_state, :waiting, %__MODULE__{data | buffer: buffer},
     [{:state_timeout, 1, :read_response}]}
  end

  def handle_event(_call_type, :read_response, :waiting, data) do
    spawn(fn -> read_next_line(data) end)
    {:keep_state, data, []}
  end

  def handle_event({:call, from}, {:error, :close}, _state, %{auth: auth} = data) do
    Logger.warn(
      inspect(%{error: "unexpected closed connection", data: Map.delete(data, :socket)})
    )

    LiveTrack.Supervisor.stop(auth)
    {:next_state, :closing, nil, [{:reply, from, :ok}]}
  end

  def handle_event({:call, from}, :check_state, state, data),
    do: {:keep_state, data, [{:reply, from, %{data: data, state: state}}]}

  def handle_event(_, :finalize, _, %{socket: socket, buffer: buffer, auth: auth}) do
    buffer = Utils.run_buffer(socket, buffer)
    apply(buffer.type, :finalize, [buffer, socket])
    LiveTrack.Supervisor.stop(auth)
    # {:keep_state, %{}, []}
  end

  def handle_event(_any, event, state, data) do
    Logger.warn(inspect(%{event: event, state: state, data: data}))

    {:keep_state, data, []}
  end

  @doc """
  Функция считывающая следующий в очереди пакет.
  """
  def read_next_line(%__MODULE__{socket: socket, auth: auth, buffer: buffer}) do
    require Logger
    prot_dir = LiveTrack.Helpers.Utils.get_dir(buffer)

    with {:ok, line} when not is_nil(line) <- LiveTrack.Helpers.Utils.read_line(socket) do
      log = %{log_type: "info", state: "received", message: inspect(line, limit: :infinity)}

      LiveTrack.Logs.do_log(log, "log", prot_dir)

      try do
        LiveTrack.Logs.do_log(log, buffer.auth.body.imei, prot_dir)
      rescue
        _ -> :ok
      end

      line
      |> Utils.split_line()
      |> Enum.reduce(buffer, fn string, buffer ->
        buffer = apply(buffer.type, :add, [buffer, string])
        Utils.run_buffer(socket, buffer)
      end)
      |> case do
        %{finish: true} ->
          self_call(auth, :finalize)

        buffer ->
          self_call(auth, {:store, buffer})
      end
    else
      e ->
        LiveTrack.Logs.do_log(%{
          log_type: "warn",
          state: "dropped",
          version: "_",
          message: "connection_dropped due to: #{inspect(e, limit: :infinity)}"
        })

        self_call(auth, :finalize)
    end
  end

  
  defp self_call(auth, params), do: LiveTrack.Supervisor.call(auth, params)
end
