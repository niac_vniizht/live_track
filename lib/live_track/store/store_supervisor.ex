defmodule LiveTrack.Store.Supervisor do
  use DynamicSupervisor
  @supervisor_name :store_supervisor
  alias LiveTrack.StoreRegistry

  def send_to_store(name, message) do
    {:ok, pid} = find_or_create(name)
    send_stored(name)
    LiveTrack.Store.Worker.send_message(pid, message)
  end

  def connect_store(name) do
    {:ok, pid} = find_or_create(name)
    LiveTrack.Store.Worker.connect(pid)
  end

  def connect_store(name, socket) do
    {:ok, pid} = find_or_create(name)
    LiveTrack.Store.Worker.connect(pid, socket)
  end

  def connect_existing_stores() do
    list_workers()
    |> Enum.map(& LiveTrack.Store.Worker.connect(&1))
  end

  def connect_existing_stores(socket) do
    list_workers()
    |> Enum.map(& LiveTrack.Store.Worker.connect(&1, socket))
  end

  def disconnect_store(name) do
    {:ok, pid} = find_or_create(name)
    LiveTrack.Store.Worker.disconnect(pid)
  end

  def disconnect_existing_stores() do
    list_workers()
    |> Enum.map(& LiveTrack.Store.Worker.disconnect(&1))
  end

  def send_stored(name) do
    {:ok, pid} = find_or_create(name)
    GenServer.cast(pid, :send_stored)
  end

  def check_store_state(name) do
    case find(name) do
      {:ok, pid} -> GenServer.call(pid, :check_state)
      e -> e
    end
  end

  def start_link(_opts) do
    DynamicSupervisor.start_link(__MODULE__, [], name: @supervisor_name)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one, extra_arguments: [])
  end

  def stop_supervisor(timeout \\ 5000) do
    Supervisor.stop(__MODULE__, :normal, timeout)
  end

  defp start(name) do
    worker_name = {:via, Registry, {StoreRegistry, name}}
    DynamicSupervisor.start_child(@supervisor_name, {LiveTrack.Store.Worker, worker_name})
  end

  def stop(name) do
    case find(name) do
      {:ok, pid} ->
        DynamicSupervisor.terminate_child(@supervisor_name, pid)

      e ->
        e
    end
  end

  def list_workers do
    StoreRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Enum.map(&elem(&1, 1))
  end

  def send_stores_to_dets do
    StoreRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [:"$1"]}])
    |> Enum.map(& LiveTrack.Store.EtsFuncs.send_all_to_dets(&1))
  end

  def find_or_create(name) do
    case Registry.lookup(StoreRegistry, name) do
      [] -> {:ok, _pid} = start(name)
      [{pid, nil}] -> {:ok, pid}
    end
  end

  def find(name) do
    case Registry.lookup(StoreRegistry, name) do
      [] -> {:error, :not_exist}
      [{pid, nil}] -> {:ok, pid}
    end
  end
end
