defmodule LiveTrack.Store.EtsFuncs do
  # @name :pack_store

  def create_table_if_not_exist(store_name) do
    unless table_exist(store_name) do
      :ets.new(store_name, [:ordered_set, :public, :named_table])
    end
  end

  def push(store_name, msg) do
    key = String.to_atom("a_#{NaiveDateTime.utc_now() |> NaiveDateTime.to_iso8601()}")
    with {:table_exist, true} <- {:table_exist, table_exist(store_name)} do
      if size(store_name) == ets_max_size() do
        send_first_to_dets(store_name)
      end
      :ets.insert_new(store_name, {key, msg})
    else
      {:table_exist, false} ->
        :ets.new(store_name, [:ordered_set, :public, :named_table])
        :ets.insert_new(store_name, {key, msg})
    end
  end

  def pop(store_name) do
    case last_key(store_name) do
      nil -> nil
      key ->
        [{_, val}] = :ets.lookup(store_name, key)
        :ets.delete(store_name, key)
        val
    end
  end

  def select_first_vals(table_name, count \\ 10) do
    keys = select_keys(table_name, count)
    vals = select_by_keys(table_name, keys)
    {keys, vals}
  end

  def select_keys(table_name, count \\ 10) do
    eot = :"$end_of_table"
    Stream.resource(
      fn -> [] end,
      fn acc ->
        case acc do
          [] ->
            case :ets.first(table_name) do
              ^eot -> {:halt, acc}
              first_key -> {[first_key], first_key}
            end

          acc ->
            case :ets.next(table_name, acc) do
              ^eot -> {:halt, acc}
              next_key -> {[next_key], next_key}
            end
        end
      end,

      fn _acc -> :ok end
    )
    |> Enum.sort()
    |> Enum.slice(0..count-1)
  end

  def select_by_keys(table_name, keys) do
    Enum.map(keys, fn key ->
      [{_, val}] = :ets.lookup(table_name, key)
      val
    end)
  end


  def delete_by_keys(store_name, keys) do
    if table_exist(store_name) do
      Enum.map(keys, & :ets.delete(store_name, &1))
    end
  end

  def send_all_to_dets(store_name) do
    LiveTrack.Store.DetsFuncs.insert(store_name, select_all(store_name))
    :ets.delete_all_objects(store_name)
  end

  def send_first_to_dets(store_name) do
    case first_key(store_name) do
      nil -> nil
      key ->
        [{_, val}] = :ets.lookup(store_name, key)
        LiveTrack.Store.DetsFuncs.insert(store_name, key, val)
        :ets.delete(store_name, key)
    end
  end

  def select_last(store_name) do
    case last_key(store_name) do
      nil -> nil
      key -> :ets.lookup(store_name, key)
    end
  end

  def last_key(store_name) do
    with true <- table_exist(store_name),
        key when key != :"$end_of_table" <- :ets.last(store_name) do
      key
    else
      _e -> nil
    end
  end

  def first_key(store_name) do
    with true <- table_exist(store_name),
        key when key != :"$end_of_table" <- :ets.first(store_name) do
      key
    else
      _e -> nil
    end
  end

  def select_all(store_name) do
    if table_exist(store_name) do
      :ets.tab2list(store_name)
    else
      []
    end
  end

  def size(store_name) do
    if table_exist(store_name), do: Keyword.get(:ets.info(store_name), :size), else: 0
  end

  def table_exist(store_name), do: :ets.whereis(store_name) != :undefined

  def ets_max_size() do
    if Application.get_env(:server, :env) == :test do
      50
    else
      10000
    end
  end
end
