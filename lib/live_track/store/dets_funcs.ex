defmodule LiveTrack.Store.DetsFuncs do
  # @name :disk_pack_store

  def create_table_if_not_exist(store_name) do
    unless table_exist(store_name) do
      if !File.exists?("priv/store"), do: File.mkdir("priv/store")
      :dets.open_file(store_name, [type: :set, file: 'priv/store/#{store_name}'])
    end
  end

  def insert(store_name, list) when is_list(list) do
    create_table_if_not_exist(store_name)
    Enum.each(list, fn
      {key, value} ->
        :dets.insert_new(store_name, {key, value})
      el ->
        IO.inspect(el, label: :dets)
    end)
  end

  def insert(store_name, key, msg) do
    create_table_if_not_exist(store_name)
    :dets.insert_new(store_name, {key, msg})
  end

  def select_first_vals(table_name, count \\ 10) do
    keys = select_keys(table_name, count)
    vals = select_by_keys(table_name, keys)
    {keys, vals}
  end

  def select_keys(table_name, count \\ 10) do
    eot = :"$end_of_table"
    Stream.resource(
      fn -> [] end,
      fn acc ->
        case acc do
          [] ->
            case :dets.first(table_name) do
              ^eot -> {:halt, acc}
              first_key -> {[first_key], first_key}
            end

          acc ->
            case :dets.next(table_name, acc) do
              ^eot -> {:halt, acc}
              next_key -> {[next_key], next_key}
            end
        end
      end,

      fn _acc -> :ok end
    )
    |> Enum.sort()
    |> Enum.slice(0..count-1)
  end

  def select_by_keys(table_name, keys) do
    Enum.map(keys, fn key ->
      [{_, val}] = :dets.lookup(table_name, key)
      val
    end)
  end

  def select_first(store_name) do
    case first_key(store_name) do
      nil -> nil
      key -> :dets.lookup(store_name, key)
    end
  end

  def select(store_name, key), do: if table_exist(store_name), do: :dets.lookup(store_name, key)

  def select_all(store_name) do
    if table_exist(store_name) do
      :dets.match(store_name, :"$1") |> List.flatten()
    else
      []
    end
  end

  def delete_by_keys(store_name, keys) do
    if table_exist(store_name) do
      Enum.map(keys, & :dets.delete(store_name, &1))
    end
  end

  def delete(store_name, key), do: if table_exist(store_name), do: :dets.delete(store_name, key)

  def first_key(store_name) do
    with true <- table_exist(store_name),
        key when key != :"$end_of_table" <- :dets.first(store_name) do
      key
    else
      _e ->  nil
    end
  end

  def table_exist(store_name) do
    :dets.first(store_name)
    true
  rescue
    _ -> false
  end
end
