defmodule LiveTrack.Store.Worker do
  use GenServer
  @conn_type :tcp
  # @name  "store_server_#{Node.self()}" |> String.to_atom()


  def send_message(pid, msg), do: GenServer.cast(pid, {:send, msg})
  def connect(pid), do: GenServer.call(pid, :connect)
  def connect(pid, socket), do: GenServer.call(pid, {:connect, socket})
  def disconnect(pid), do: GenServer.call(pid, :disconnect)

  def start_link(name), do: start(name)

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent
    }
  end

  def start({:via, Registry, {_registry_name, name}} = worker_name) do
    store_name = if is_atom(name), do: name, else: inspect(name) |> String.to_atom()
    LiveTrack.Store.EtsFuncs.create_table_if_not_exist(store_name)
    LiveTrack.Store.DetsFuncs.create_table_if_not_exist(store_name)
    store_flag = case LiveTrack.Client.is_user_active("store", "comm_client_service") do
      {:ok, true} -> :send
      _ -> :store
    end
    GenServer.start_link(__MODULE__, %{store_flag: store_flag, store_name: store_name}, name: worker_name)
  end

  # def start_link(_), do: GenServer.start_link(__MODULE__, %{store_flag: :store}, name: @name)

  def init(state) do
    {:ok, state}
  end

  def handle_call(:connect, _from, %{store_name: store_name} = state) do
    state =
      state
      |> Map.put(:store_flag, :send)

    LiveTrack.Store.Supervisor.send_stored(store_name)

    {:reply, :ok, state}
  end

  def handle_call({:connect, socket}, _from, %{store_name: store_name} = state) do
    state =
      state
      |> Map.put(:store_flag, :send)
      |> Map.put(:socket, socket)

    LiveTrack.Store.Supervisor.send_stored(store_name)

    {:reply, :ok, state}
  end

  def handle_call(:disconnect, _from, state) do
    state =
      state
      |> Map.put(:store_flag, :store)
      {:reply, :ok, state}
  end

  def handle_call(:check_state, _from, state) do
    {:reply, state, state}
  end

  def handle_cast({:send, msg}, %{store_flag: :store, store_name: store_name} = state) do
    LiveTrack.Store.EtsFuncs.push(store_name, msg)
    if LiveTrack.Store.EtsFuncs.size(store_name) == LiveTrack.Store.EtsFuncs.ets_max_size() do
      LiveTrack.Store.EtsFuncs.send_all_to_dets(store_name)
    end
    {:noreply, state}
  end

  def handle_cast({:send, msg}, %{store_flag: :send, store_name: store_name} = state) do
    case send_msg(msg, state) do
      :ok -> {:noreply, state}
      e ->
        IO.inspect(e)
        LiveTrack.Store.EtsFuncs.push(store_name, msg)
        {:noreply, Map.put(state, :store_flag, :store)}
    end
  end

  def handle_cast(:send_stored, %{store_flag: :send, store_name: store_name} = state) do
    case send_dets_msg(store_name, state) do
      :empty -> case send_ets_msg(store_name, state) do
        :error ->
          {:noreply, Map.put(state, :store_flag, :store)}
        _ -> {:noreply, state}
      end
      :error ->
        {:noreply, Map.put(state, :store_flag, :store)}
      _ -> {:noreply, state}
    end
  end

  def handle_cast(:send_stored, state) do
    {:noreply, state}
  end

  defp send_ets_msg(store_name, state) do
    case LiveTrack.Store.EtsFuncs.select_first_vals(store_name, 10) do
      {[], _} -> :empty
      {keys, vals} ->
        msg = LiveTrack.Transform.Server.transform_store_messages(vals, keys, store_name, "ets")
        case send_msg(msg, state) do
          :ok ->
            :sended
          e ->
            IO.inspect(e)
            :error
        end
    end
  end

  defp send_dets_msg(store_name, state) do
    case LiveTrack.Store.DetsFuncs.select_first_vals(store_name, 10) do
      {[], _} -> :empty
      {keys, vals} ->
        msg = LiveTrack.Transform.Server.transform_store_messages(vals, keys, store_name, "dets")
        case send_msg(msg, state) do
          :ok ->
            :sended
          e ->
            IO.inspect(e)
            :error
        end
    end
  end

  def send_msg(message, state) do
    if Application.get_env(:server, :env) == :test do
      :ok
    else
      LiveTrack.Client.send_message_to_chat(message, "store", "live_track", @conn_type, state)
    end
  end
end
