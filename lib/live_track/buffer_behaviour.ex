defmodule LiveTrack.BufferBehaviour do
  @callback new(line :: binary(), opts :: list()) :: {:ok, any()} | {:error, any()}
  @callback add(buffer :: struct(), line :: binary()) :: struct()
  @callback finalize(buffer :: struct(), socket :: any()) :: any()
  @callback send_data(buffer :: struct()) :: any()
end
