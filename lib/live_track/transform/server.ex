defmodule LiveTrack.Transform.Server do
  use GenServer
  @name  "transform_server_#{Node.self()}" |> String.to_atom()

  def transform_buffer_message(message, buffer_name, dt, transform_type \\ :mapify) do
    GenServer.call(@name, {:transform_buffer_message, message, buffer_name, dt, transform_type})
  end

  def transform_store_messages(messages, ids, store_name, store_type, transform_type \\ :mapify) do
    GenServer.call(@name, {:transform_store_messages, messages, ids, store_name, store_type, transform_type})
  end

  def start_link(_), do: GenServer.start_link(__MODULE__, %{}, name: @name)

  def init(state) do
    {:ok, state}
  end

  def handle_call({:transform_buffer_message, message, buffer_name, dt, transform_type}, _from, state) do
    {
      :reply,
      prepare_message(transform_type, %{data: message, buffer_name: buffer_name, dt: dt}),
      state
    }
  end

  def handle_call({:transform_store_messages, messages, ids, store_name, store_type, transform_type}, _from, state) do
    {
      :reply,
      prepare_message(transform_type, %{
        data_list: messages,
        ids: ids,
        store_name: store_name,
        store_type: store_type
      }),
      state
    }
  end

  defp prepare_message(:zip, message),
    do: Poison.encode!(message) |> :zlib.gzip() |> Base.encode64

  defp prepare_message(:mapify, message),
    do: Poison.encode!(message)

end
