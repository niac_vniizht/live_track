defmodule TestCmd do
  @spec factorial(non_neg_integer) :: pos_integer
  def factorial(0), do: 1
  def factorial(i) when i > 0, do: i * factorial(i-1)
end
