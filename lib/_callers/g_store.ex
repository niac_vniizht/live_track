# defmodule Server.GStore do
#   @moduledoc """
#   Хранилище
#   """
#   use Munin.Service.Global,
#     service: "live_track",
#     params: %{}

#   def get_service_data(service, key) do
#     :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :value, [service, key])
#   rescue
#     _e -> nil
#   end

#   def get_service_addr(service) do
#     %{service: service, machine_addr: machine_addr} = get_service_data(service, "init_params")

#     "#{service}@#{machine_addr}"
#   rescue
#     _e -> nil
#   end

#   def get_db(service, key) do
#     resp = get_service_data(service, key)

#     [
#       hostname: resp.hostname,
#       username: resp.username,
#       password: resp.password,
#       database: resp.database,
#       port: resp.port
#     ]
#   rescue
#     _e -> nil
#   end
# end
