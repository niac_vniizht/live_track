defmodule Server.RFiler do
  @moduledoc """
  files
  """

  def read_file(filename) do
    {:ok, file} =
      File.cwd!()
      |> Path.join(filename)
      |> File.read()

    {:ok, file}
  rescue
    _e ->
      {:ok, ""}
  end

  def remove_file(filename) do
    :ok =
      File.cwd!()
      |> Path.join(filename)
      |> File.rm
  rescue
    _e ->
      :error
  end

  def count_logs(filename) do
    len =
      read_file(filename)
      |> elem(1)
      |> Enum.split("\n")
      |> length()

    {:ok, len}
  end

  def read_file_and_remove(filename) do
    file = read_file(filename)
    remove_file(filename)

    {:ok, file}
  end

  def write_file(data, filename, ext) do
    :ok =
      File.cwd!()
      |> Path.join("#{filename}.#{ext}")
      |> File.write(data)

    {:ok, "#{filename}.#{ext}"}
  end
end