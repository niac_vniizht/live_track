defmodule Server do
  @moduledoc """
  Server keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
  use Application

  @impl true
  def start(_type, _args), do: Server.Application.start_link([])

  @impl true
  def prep_stop(_state) do
    LiveTrack.Store.Supervisor.send_stores_to_dets()
    IO.inspect(:stop)
    :ok
  end
end
