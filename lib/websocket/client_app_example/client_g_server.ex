defmodule LiveTrack.ClientP do
  use GenServer
  require Logger

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def run(%{"response" => msg}) do
    GenServer.cast(__MODULE__, {:run, msg})
  end

  @impl true
  def init(stack) do
    {:ok, stack}
  end

  @impl true
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  @impl true
  def handle_cast({:run, "pong"}, %{pong: :done} = state) do
    Logger.debug("Мы сервер уже проверяли!")
    {:noreply, state}
  end
  def handle_cast({:run, "pong"}, state) do
    Logger.debug("Команда echo работает отлично! зАПУСКАЙТЕ БЫЧКА!")
    {:noreply, %{pong: :done}}
  end
  def handle_cast(:run, state) do
    echo()
    {:noreply, state}
  end


  def send_connect do
    WebSockex.send_frame(client_pid(), {:text, Jason.encode!(%{"command" => "join", "room" => "machines"})})
  end

  def echo() do
    WebSockex.send_frame(client_pid(), {:text, Jason.encode!(%{"command" => "ping"})})
  end

  defp client_pid do
    [{"client", client}] = :ets.lookup(Application.get_env(:server, :ets_storage), "client")
    client
  rescue
    _e -> nil
  end
end
