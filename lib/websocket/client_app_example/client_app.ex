defmodule LiveTrack.ClientApp do
  use WebSockex
  require Logger

  def start_link([url: url]), do: start(url, [])

  @doc """
  :ets.new(Application.get_env(:server, :ets_storage), [:set, :public, :named_table])
  LiveTrack.ClientP.start_link([])
  LiveTrack.ClientApp.start_link([url: "ws://172.25.78.151:4001/ws/chat?access_token=exbe0Oiims4mBZLc"])
  LiveTrack.ClientP.echo
  """
  def start(url, state) do
    # {:ok, pid} = WebSockex.start(url, __MODULE__, state)

    # Logger.warn("Client pid: #{inspect pid}")
    # :ets.insert(Application.get_env(:server, :ets_storage), {"client", pid})
    # {:ok, pid}
    WebSockex.start(url, __MODULE__, state)
  end

  def terminate(reason, state) do
    IO.puts("WebSockex for remote debbugging on port #{state.port} terminating with reason: #{inspect reason}")
    exit(:normal)
  end


  @doc """
  Заглушка в ошибке
  """
  def handle_frame({:text, msg}, state) do


    Logger.warn("Client: #{msg}")
    # resp = read_message(Jason.decode(msg))
    # Logger.warn("Client pid: #{inspect resp}")
    # Process.send_after(self(), {:send, %{id: 1, method: "Network.connect", params: %{}}}, 2000)
    # LiveTrack.ClientP.run(Jason.decode!(msg))


    {:ok, state}
  rescue
    _e ->
      Logger.error("Error, massage can't be parse via Jason library. message: #{inspect(msg, limit: :infinity)}")
      {:ok, state}
  end
end
