defmodule LiveTrackWS.SocketHandler do
  @behaviour :cowboy_websocket

  alias LiveTrack.Client

  def init(request, state) do
    token = get_access_token(request)

    case Client.validate_access_token(token) do
      {:ok, session} -> {:cowboy_websocket, request, session, %{idle_timeout: 10 * 60 * 1000}}
      {:error, _} -> {:ok, :cowboy_req.reply(400, request), state}
    end
  end

  @spec websocket_init(any) :: {:ok, any}
  def websocket_init(session_id) do
    Client.subscribe_to_user_session(self(), session_id)

    {:ok, session_id}
  end

  def websocket_handle({:text, command_as_json}, session_id) do
    case Poison.decode(command_as_json) do
      {:error, _reason} -> {:ok, session_id}
      {:ok, command} -> LiveTrack.Handler.handle(command, session_id)
    end
  end

  def websocket_handle(_message, session_id) do
    {:ok, session_id}
  end

  def websocket_info(message, session_id) do
    message = case message do
      {:EXIT, _, _} -> :EXIT
      _ -> message
    end
    {:reply, {:text, Poison.encode!(message)}, session_id}
  end



  @spec get_access_token(:cowboy_req.req()) :: binary() | nil
  def get_access_token(req) do
    case Enum.find(:cowboy_req.parse_qs(req), & elem(&1, 0) == "access_token") do
      {"access_token", access_token}  -> access_token
      _                               -> nil
    end
  end

  def terminate(_session_info, _params, session_id) do
    Client.exit_rooms(session_id)
    :ok
  end
end
