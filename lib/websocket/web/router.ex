defmodule LiveTrackWS.Router do
  use Plug.Router
  require EEx

  plug Plug.Static,
    at: "/",
    from: :server

  plug :match
  plug :dispatch

  plug Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Poison
  
  
  EEx.function_from_file(:defp, :application_html, "lib/websocket/web/templates/application.html.eex", [])
  EEx.function_from_file(:defp, :app_x, "lib/websocket/web/templates/chat.html.eex", [])

  get "/" do
    send_resp(conn, 200, application_html())
  end
  get "/chat" do
    send_resp(conn, 200, app_x())
  end

  match _ do
    send_resp(conn, 404, "404")
  end

  def dispatch do
    [
      {:_,
        [
          {"/ws/chat/", LiveTrackWS.SocketHandler, []},
          {:_, Plug.Cowboy.Handler, {__MODULE__, []}}
        ]
      }
    ]
  end
end