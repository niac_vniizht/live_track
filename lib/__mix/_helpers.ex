defmodule HelperMix do
  @moduledoc """
  Just some funcs
  """

  def read_options(opts, avaliable_options) do
    Enum.reduce(opts, [], fn
      item, [] ->
        true = validate_option!(item, avaliable_options)
        [[item]]

      item, [hd | tail] = acc ->
        if String.match?(item, ~r/^-[[:alnum:]]$/) do
          true = validate_option!(item, avaliable_options)
          [[item] | acc]
        else
          [[item | hd] | tail]
        end
    end)
    |> Enum.map(&Enum.reverse/1)
    |> Enum.sort_by(fn [curr_option | _option_param] -> 
      Enum.find_index(avaliable_options, fn opt -> curr_option == opt end)
    end, :asc)
  end

  defp validate_option!(type, avaliable_options) do
    if type in avaliable_options do
      true
    else
      raise(
        ArgumentError,
        "Неизвестный тип параметра - #{inspect(type, pretty: true, limit: :infinity)}"
      )
    end
  end

  def cmd(list) when is_list(list),
    do: Enum.each(Enum.with_index(list, 1), &cmd(&1, length(list)))

  def cmd(command) when is_binary(command), do: Mix.Shell.cmd(command, [], &IO.puts(&1))

  def cmd({command, idx}, total) when is_binary(command) do
    IO.write("Команда: #{idx}/#{total} \r")
    cmd(command)
    :ok
  end
end
