defmodule Mix.Tasks.Pull do
  @moduledoc """
  Pulls from all repositories
  """
  use Mix.Task
  import HelperMix

  def run(_) do
    cmd(commands())
    IO.puts("Данные получены из системы git.")
  end

  def commands() do
    [
      "git pull origin master --quiet",
      # "git pull gitlab master --quiet"
    ]
  end
end
