defmodule LiveTrack.Room do
  use GenServer

  alias LiveTrack.UserSessionSupervisor

  defstruct sessions: [], name: nil

  #
  # Клиентские функции
  #

  def join(pid, session_id), do: GenServer.call(pid, {:join, session_id})
  def is_user_active(pid, session_id), do: GenServer.call(pid, {:is_user_active, session_id})
  def reject_user(pid, session_id), do: GenServer.cast(pid, {:reject_user, session_id})

  def send(pid, message, session_id, :ws, _) do
    :ok = GenServer.call(pid, {:send, message, session_id, :ws})
  end

  def send(pid, message, session_id, :tcp, %{socket: socket}) do
    :ok = GenServer.call(pid, {:send, message, session_id, :tcp, socket})
  end

  #
  # Серверные функции
  #

  def start_link(name), do: create(name)
  def create(name = {:via, Registry, {_registry_name, room_name}}) do
    GenServer.start_link(__MODULE__, %__MODULE__{name: room_name}, name: name)
  end
  def create(room_name), do: GenServer.start_link(__MODULE__, %__MODULE__{name: room_name}, name: String.to_atom(room_name))

  def init(state), do: {:ok, state}

  def handle_call({:join, session_id}, _from, state) do
    {message, new_state} = case joined?(state.sessions, session_id) do
      true -> {{:error, :already_joined}, state}
      false -> {:ok, add_session(state, session_id)}
    end

    {:reply, message, new_state}
  end

  def handle_call({:send, message, session_id, :ws}, _from, state = %__MODULE__{name: name}) do
    Enum.each(state.sessions, &UserSessionSupervisor.notify(%{from: session_id, room: name, message: message, conn_type: :ws}, &1))
    {:reply, :ok, state}
  end

  def handle_call({:send, message, session_id, :tcp, socket}, _from, state = %__MODULE__{name: name}) do
    Enum.each(state.sessions, &UserSessionSupervisor.notify(%{from: session_id, room: name, message: message, conn_type: :tcp, socket: socket}, &1))
    {:reply, :ok, state}
  end

  def handle_call({:is_user_active, session_id}, _from, state = %__MODULE__{sessions: sessions}) do
    {:reply, {:ok, joined?(sessions, session_id)}, state}
  end

  def handle_cast({:reject_user, session_id}, %__MODULE__{sessions: sessions} = state) do
    state =
      %__MODULE__{state | sessions: Enum.reject(sessions, & &1 == session_id)}

    {:noreply, state}
  end

  #
  # Приватные функции
  #

  defp joined?(sessions, session_id), do: Enum.member?(sessions, session_id)

  defp add_session(state = %__MODULE__{sessions: sessions}, session_id) do
    %__MODULE__{state | sessions: Enum.uniq([session_id|sessions])}
  end
end
