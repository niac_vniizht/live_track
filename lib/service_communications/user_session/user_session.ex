defmodule LiveTrack.UserSession do
  use GenServer

  defstruct clients: []

  def subscribe(pid, client_pid), do: GenServer.call(pid, {:subscribe, client_pid})
  def sessions(pid), do: GenServer.call(pid, :sessions)
  @spec notify(atom | pid | {atom, any} | {:via, atom, any}, map()) :: :ok
  def notify(pid, message), do: GenServer.cast(pid, {:send, message})

  #
  # Серверные функции
  #
  def start_link(name) do
    GenServer.start_link(__MODULE__, %__MODULE__{}, name: name)
  end
  def init(state), do:  {:ok, state}

  def handle_call({:subscribe, client_pid}, _from, state), do: {:reply, :ok, %__MODULE__{state | clients: [client_pid|state.clients]} }

  def handle_call(:sessions, state) do
    {:reply, state, state}
  end

  def handle_cast({:send, %{message: _, conn_type: :ws} = message}, state) do
    Enum.each(state.clients, &Kernel.send(&1, message))
    {:noreply, state}
  end

  def handle_cast({:send, %{message: _, conn_type: :tcp, socket: socket} = message}, state) do
    :gen_tcp.send(socket, Jason.encode!(Map.drop(message, [:conn_type, :socket]) ) <> "\r\n")
    {:noreply, state}
  end
end
