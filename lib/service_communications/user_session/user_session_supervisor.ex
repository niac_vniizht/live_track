defmodule LiveTrack.UserSessionSupervisor do
  use DynamicSupervisor

  alias LiveTrack.{UserSession, UserSessionRegistry, TokenRepository}

  #
  # Клиентские функции
  #

  def create(session_id) do
    case find(session_id) do
      nil   ->
        start(session_id)
        :ok
      pid when is_pid(pid)  -> {:error, :already_exists}
    end
  end

  def subscribe(client_pid, [to: session_id]) do
    case find(session_id) do
      nil -> {:error, :session_not_exists}
      pid -> UserSession.subscribe(pid, client_pid)
    end
  end

  @spec notify(map(), any) :: :ok | {:error, :session_not_exists}
  def notify(message, session_id) do
    case find(session_id) do
      nil -> {:error, :session_not_exists}
      pid -> UserSession.notify(pid, message)
    end
  end

  def clean(session_id) do
    case find(session_id) do
      nil -> {:error, :session_not_exists}
      pid -> UserSession.sessions(pid)
    end
  end

  #
  # Серверные функции
  #

  def start_link(opts \\ []) do
    DynamicSupervisor.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  defp start(session_id) do
    try do
      name = {:via, Registry, {UserSessionRegistry, session_id}}
      DynamicSupervisor.start_child(__MODULE__, {UserSession, name})
    rescue
      _e -> {:error, :already_exists}
    end
  end

  def find(session_id) do
    with {_key, session} <- Enum.find(TokenRepository.list_tokens, & elem(&1, 1) == session_id),
                  [{pid, _}]  <- Registry.lookup(UserSessionRegistry, session) do
      pid
    else
      _e -> nil
    end
  end
end
