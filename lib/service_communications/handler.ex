defmodule LiveTrack.Handler do

  alias LiveTrack.Client

  def handle(%{"command" => "join", "room" => room}, %{session_id: session_id, conn_type: conn_type} = attrs) do
    Client.exit_rooms(session_id)
    case Client.join_room(room, session_id, attrs) do
      :ok ->
        spawn(fn ->
          if room == "store" and session_id == "comm_client_service" do
            case conn_type do
              :ws -> LiveTrack.Store.Supervisor.connect_existing_stores()
              :tcp -> LiveTrack.Store.Supervisor.connect_existing_stores(Map.get(attrs, :socket))
            end

            :ok
          end
        end)

        {:ok, session_id}
      {:error, message} ->
        {:reply, {:text, Poison.encode!(%{error: message})}, session_id}
    end
  end

  def handle(%{"command" => "join", "rooms" => rooms}, %{session_id: session_id, conn_type: conn_type} = attrs) do
    Client.exit_rooms(session_id)
    case Client.join_rooms(rooms, session_id, attrs) do
      :ok ->
        spawn(fn ->
          if "store" in rooms  and session_id == "comm_client_service" do
            case conn_type do
              :ws -> LiveTrack.Store.Supervisor.connect_existing_stores()
              :tcp -> LiveTrack.Store.Supervisor.connect_existing_stores(Map.get(attrs, :socket))
            end
            :ok
          end
        end)

        {:ok, session_id}
      {:error, message} ->
        {:reply, {:text, Poison.encode!(%{error: message})}, session_id}
    end
  end

  # def handle(command = %{"command" => "join"}, session_id) do
  #   handle(Map.put(command, "room", "init"), session_id)
  # end

  def handle(%{"room" => room, "message" => message}, %{session_id: session_id, conn_type: :ws}) do
    case Client.send_message_to_chat(message, room, session_id, :ws, %{}) do
      {:error, message} ->
        {:reply, {:text, Poison.encode!(%{ error: message })}, session_id}
      :ok ->
        {:ok, session_id}
    end
  end

  def handle(%{"room" => room, "message" => message}, %{session_id: session_id, conn_type: :tcp, socket: socket}) do
    case Client.send_message_to_chat(message, room, session_id, :tcp, %{socket: socket}) do
      {:error, message} ->
        {:reply, {:text, Poison.encode!(%{ error: message })}, session_id}
      :ok ->
        {:ok, session_id}
    end
  end

  def handle(%{"command" => "remove_from_store", "ids" => ids, "store_name" => store_name, "store_type" => store_type}, state) do
    ids = Enum.map(ids, & String.to_atom(&1))
    case store_type do
      "ets" -> LiveTrack.Store.EtsFuncs.delete_by_keys(String.to_atom(store_name), ids)
      "dets" -> LiveTrack.Store.DetsFuncs.delete_by_keys(String.to_atom(store_name), ids)
      _ -> :ok
    end
    LiveTrack.Store.Supervisor.send_stored(String.to_atom(store_name))
    {:ok, state}
  end

  def handle(%{"command" => "version"}, %{session_id: session_id, conn_type: conn_type} = state) do
    version = "0.1.1"
    case Client.send_message_to_chat("server version: #{version}", "cmd", "live_track", conn_type, state) do
      {:error, message} ->
        {:reply, {:text, Poison.encode!(%{ error: message })}, session_id}
      :ok ->
        {:ok, session_id}
    end

  rescue
    e ->
      IO.inspect(e)
      {:ok, session_id}
  end

  def handle(%{"command" => "system_cmd", "system_cmd" => system_cmd, "args" => args, "opts" => opts}, %{session_id: session_id, conn_type: conn_type} = state) do
    opts = case opts do
      [] -> []
      %{} = map -> Map.to_list(map)
    end
    message =
      System.cmd(system_cmd, args, opts)
      |> inspect()

    case Client.send_message_to_chat(message, "cmd", "live_track", conn_type, state) do
      {:error, message} ->
        {:reply, {:text, Poison.encode!(%{ error: message })}, session_id}
      :ok ->
        {:ok, session_id}
    end

  rescue
    e ->
      IO.inspect(e)
      {:ok, session_id}
  end

  def handle(%{
    "command" => "cmd",
    "module" => module,
    "function" => function,
    "attrs_list" => attrs_list
  }, %{session_id: session_id, conn_type: conn_type} = state) do
    with {:try_execute, val} <- {:try_execute, apply(String.to_existing_atom(module), String.to_atom(function), attrs_list)},
    {:try_json_encode, {:ok, val}} <- {:try_json_encode, Jason.encode(val)} do

      Client.send_message_to_chat(val, "cmd", "live_track", conn_type, state)
    else
      {:try_execute, err} ->
        Client.send_message_to_chat("can`t be executed: #{inspect err}", "cmd", "live_track", conn_type, state)
      {:try_json_encode, _err} ->
        Client.send_message_to_chat(inspect(apply(String.to_existing_atom(module), String.to_atom(function), attrs_list)), "cmd", "live_track", conn_type, state)
      _ ->
        {:reply, {:text, Poison.encode!(%{ error: "unexpected error" })}, session_id}
    end
  rescue
    e ->
      IO.inspect(e)
      {:ok, session_id}
  end

  def handle(%{"command" => "ping"}, session_id) do
    response = %{response: "pong"}

    {:reply, {:text, Poison.encode!(response)}, session_id}
  end

  def handle(%{"command" => "create", "room" => room}, session_id) do
    response = case Client.create_room(room) do
      {:ok, message} -> %{success: message}
      {:error, message} -> %{error: message}
    end

    {:reply, {:text, Poison.encode!(response)}, session_id}
  end

  def handle(_not_handled_command, session_id), do: {:ok, session_id}
end
