defmodule LiveTrack.Client do
  alias LiveTrack.{RoomSupervisor, UserSessionSupervisor, TokenRepository}

  @spec create_room(binary()) :: {:error, binary()} | {:ok, binary()}
  def create_room(room) do
    case RoomSupervisor.create(room) do
      {:ok, _}                  -> {:ok, "Комната #{room} была создана"}
      {:error, :already_exists} -> {:error, "#{room} уже существует"}
    end
  end

  @spec join_room(binary(), binary(), map()) :: :ok | {:error, any()}
  def join_room(room, user_id, attrs) do
    case RoomSupervisor.join(room, as: user_id) do
      :ok ->
        UserSessionSupervisor.notify(%{room: room, message: "К комнате #{room} подключился #{user_id}!"} |> Map.merge(attrs), user_id)
        :ok
      {:error, :already_joined}   -> {:error, "Вы уже присоединились к #{room} комнате!"}
      {:error, :unexisting_room}  -> {:error, "Комната #{room} не существует"}
    end
  end

  @spec join_rooms(list(), binary(), map()) :: :ok | {:error, any()}
  def join_rooms(rooms, user_id, attrs) do
    Enum.map(rooms, fn room ->
      case RoomSupervisor.join(room, as: user_id) do
        :ok ->
          %{room: room, message: "К комнате #{room} подключился #{user_id}!"}
          |> Map.merge(attrs)
          |> UserSessionSupervisor.notify(user_id)
          :ok
        {:error, :already_joined}   -> {:error, "Вы уже присоединились к #{room} комнате!"}
        {:error, :unexisting_room}  -> {:error, "Комната #{room} не существует"}
      end
    end)
    |> Enum.reject(& &1 == :ok)
    |> case do
      [] -> :ok
      errors ->
        {:error, Enum.map(errors, fn {:error, error} -> error end) |> inspect()}
    end
  end

  def exit_rooms(user_id) do
    RoomSupervisor.list_rooms
    |> Enum.map(fn
      "store" ->
        spawn(fn ->
          if user_id == "comm_client_service" do
            LiveTrack.Store.Supervisor.disconnect_existing_stores()
            :ok
          end
        end)
        RoomSupervisor.reject_user("store", as: user_id)
      room ->
        RoomSupervisor.reject_user(room, as: user_id)
    end)
    :ok
  end

  def is_user_active(room, user), do: LiveTrack.RoomSupervisor.is_user_active(room, as: user)

  @spec send_message_to_chat(binary(), binary(), binary(), atom(), map()) :: :ok | {:error, binary()}
  def send_message_to_chat(message, room, user_id, conn_type, attrs) do
    case RoomSupervisor.send(message, room, user_id, conn_type, attrs) do
      :ok                         -> :ok
      {:error, :unexisting_room}  -> {:error, "#{room} does not exists"}
    end
  end

  @spec subscribe_to_user_session(any, any) :: any
  def subscribe_to_user_session(subscriber_pid, user_id), do: UserSessionSupervisor.subscribe(subscriber_pid, to: user_id)


  def validate_access_token(access_token) do
    case TokenRepository.find_user_session_by(access_token) do
      nil     -> {:error, :access_token_invalid}
      session -> {:ok, session}
    end
  end
end
