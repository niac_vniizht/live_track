defmodule Server.Application do
  @moduledoc false
  require Logger
  use Supervisor

  @http_options [
    dispatch: LiveTrackWS.Router.dispatch(),
    port: Application.get_env(:server, :ws_port)
  ]

  def start_link(opts), do: Supervisor.start_link(__MODULE__, :ok, opts)

  @impl true
  def init(:ok) do
    children =
      List.flatten([
        # network(),
        tcp_supervisor(),
        ws_api(),
        setup(),
        store_setup()
      ])

    Supervisor.init(children, [strategy: :one_for_one, name: Server.Supervisor])
  end

  def ws_api() do

    child_sp = [scheme: :http, plug: LiveTrackWS.Router, options: @http_options]

    [
      {Registry, keys: :unique, name: LiveTrack.RoomRegistry},
      {Registry, keys: :unique, name: LiveTrack.UserSessionRegistry},
      LiveTrack.RoomSupervisor,
      LiveTrack.UserSessionSupervisor,
      LiveTrack.TokenRepository,
      # Plug.Cowboy.child_spec(child_sp),
    ]
  end

  def tcp_supervisor do
    [
      {Registry, keys: :unique, name: LiveTrack.SimpleRegistry},
      {Registry, keys: :unique, name: LiveTrack.RecRegistry},
      {Registry, keys: :unique, name: LiveTrack.StoreRegistry},
      {Task.Supervisor,
       name: LiveTrack.TaskSupervisor, max_children: Application.get_env(:server, :max_machines)},
      LiveTrack.ReceiverSupervisor,
      LiveTrack.Supervisor,
      LiveTrack.Transform.Server,
      LiveTrack.Store.Supervisor
    ]
  end

  def setup() do
    [
      LiveTrack.Setup
    ]
  end

  def store_setup() do
    if Application.get_env(:server, :env) in [:dev, :prod], do: [LiveTrack.StoreSetup], else: []
  end
end
