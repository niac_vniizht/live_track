defmodule LiveTrack.StoreSetup do
  use Task, restart: :transient
  require Logger

  @spec start_link(any) :: {:ok, pid}
  def start_link(_args), do: Task.start_link(__MODULE__, :run, [])

  @spec run :: :ok
  def run do
    LiveTrack.Store.Supervisor.find_or_create(:niac_store)
    :ok
  end

end
