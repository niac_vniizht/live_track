
defmodule LiveTrack.Setup do
  use Task, restart: :transient
  require Logger
  alias LiveTrack.ReceiverSupervisor
  alias LiveTrack.{RoomSupervisor, UserSessionSupervisor, TokenRepository}

  @spec start_link(any) :: {:ok, pid}
  def start_link(_args), do: Task.start_link(__MODULE__, :run, [])

  @spec run :: :ok
  def run do
    run_services()
    # log_addr(get_env(:ws_addr), get_env(:ws_port))
    run_rooms()

    :ok
  end

  defp run_rooms() do
    create_room("store")
    create_room("machines")
    create_room("cmd")
    create_session("alfheim_service", "exbe0Oiims4mBZLc")
    create_session("comm_client_service", "Bckef5D3RUOTWwBu")
  end

  defp create_session(client_name, token) do
    UserSessionSupervisor.create(client_name)
    TokenRepository.add(client_name, token)
    Logger.info("Определена сессия: #{client_name} с токеном #{token}")
  end

  def create_room(room_name) do
    RoomSupervisor.create(room_name)
    Logger.info("Создана комната: #{room_name}")
  end

  # for(x <- 10000000..10003000, do: LiveTrack.Setup.create_room("machine_#{x}"))
  # for(x <- 10000000..10003000, do: LiveTrack.Client.send_message_to_chat("asdsadasdasad", "store", "test_user"))
  defp run_services do
    # start_service(:wialon)
    # start_service(:mvps)
    start_service(:niac)
    # start_service(:scout)
  end

  defp start_service(atom) do
    ReceiverSupervisor.create(atom, Application.get_env(:server, :"#{atom}_port"))
  end

  # defp get_env(atom), do: Application.get_env(:server, atom)
  # defp log_addr(ip, port) do
  #   Logger.info("Запущен HTTP сервис по адресу http://#{Tuple.to_list(ip) |> Enum.join(".")}:#{port}")
  #   Logger.info("Запущен WebSocket сервис по адресу ws://#{Tuple.to_list(ip) |> Enum.join(".")}:#{port}")
  # end
end
