defmodule Test.StoreTest do
  use ExUnit.Case, async: false
  import Mock, warn: false
  alias LiveTrack.Store.{EtsFuncs, DetsFuncs}

  describe "store // " do
    setup do
      {:ok, _} = Application.ensure_all_started(:server)
      on_exit(fn -> clear(:test_store) end)
    end

    test "Проверка записи в новый стор" do
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg") == :ok
      assert LiveTrack.Store.EtsFuncs.table_exist(:test_store) == true
      assert LiveTrack.Store.Supervisor.check_store_state(:test_store) ==
        %{store_flag: :store, store_name: :test_store}
      Process.sleep(10)
      assert [{_, "msg"}] = LiveTrack.Store.EtsFuncs.select_all(:test_store)
    end

    test "Проверка записи в созданный стор" do
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg") == :ok
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg2") == :ok
      assert LiveTrack.Store.EtsFuncs.table_exist(:test_store) == true
      Process.sleep(10)
      assert [{_, "msg"}, {_, "msg2"}] = LiveTrack.Store.EtsFuncs.select_all(:test_store)
    end

    test "Проверка отсутствия записи при активном соединении" do
      assert LiveTrack.Store.Supervisor.connect_store(:test_store) == :ok
      assert LiveTrack.Store.Supervisor.check_store_state(:test_store) == %{store_flag: :send, store_name: :test_store}
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg") == :ok
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg2") == :ok
      Process.sleep(10)
      assert [] = LiveTrack.Store.EtsFuncs.select_all(:test_store)
    end

    test "Проверка записи после обрыва соединения" do
      assert LiveTrack.Store.Supervisor.connect_store(:test_store) == :ok
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg") == :ok
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg2") == :ok
      assert LiveTrack.Store.Supervisor.disconnect_store(:test_store) == :ok
      assert LiveTrack.Store.Supervisor.check_store_state(:test_store) ==
        %{store_flag: :store, store_name: :test_store}
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg3") == :ok
      assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg4") == :ok
      Process.sleep(10)
      assert [{_, "msg3"}, {_, "msg4"}] = LiveTrack.Store.EtsFuncs.select_all(:test_store)
    end

    # test "Проверка очистки ets после соединения" do
    #   assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg") == :ok
    #   assert LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg2") == :ok
    #   assert LiveTrack.Store.Supervisor.check_store_state(:test_store) ==
    #     %{store_flag: :store, store_name: :test_store}
    #   Process.sleep(10)
    #   IO.inspect(LiveTrack.Store.EtsFuncs.select_all(:test_store))
    #   assert LiveTrack.Store.Supervisor.connect_store(:test_store) == :ok
    #   assert %{store_flag: :send} = LiveTrack.Store.Supervisor.check_store_state(:test_store)
    #   Process.sleep(10)
    #   assert [] == LiveTrack.Store.EtsFuncs.select_all(:test_store)
    # end

    test "Проверка записи в dets после достижения лимита ets" do
      Enum.each(1..LiveTrack.Store.EtsFuncs.ets_max_size() + 2, fn i ->
        LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg#{i}")
      end)
      Process.sleep(100)
      assert LiveTrack.Store.EtsFuncs.select_all(:test_store) |> length() == 2
      assert LiveTrack.Store.DetsFuncs.select_all(:test_store) |> length() == LiveTrack.Store.EtsFuncs.ets_max_size()
    end

    # test "Проверка очистки хранилищ после восстановления соединения" do
    #   Enum.each(1..LiveTrack.Store.EtsFuncs.ets_max_size() + 2, fn i ->
    #     LiveTrack.Store.Supervisor.send_to_store(:test_store, "msg#{i}")
    #   end)
    #   Process.sleep(500)
    #   assert LiveTrack.Store.EtsFuncs.select_all(:test_store) != []
    #   assert LiveTrack.Store.DetsFuncs.select_all(:test_store) != []
    #   assert LiveTrack.Store.Supervisor.connect_existing_stores() == [:ok]
    #   Process.sleep(500)
    #   assert LiveTrack.Store.EtsFuncs.select_all(:test_store) == []
    #   assert LiveTrack.Store.DetsFuncs.select_all(:test_store) == []
    # end
  end

  def clear(store_name) do
    LiveTrack.Store.Supervisor.stop(store_name)
    if EtsFuncs.table_exist(store_name), do: :ets.delete(store_name)
    if DetsFuncs.table_exist(store_name) do
      :dets.delete_all_objects(store_name)
      :dets.close(store_name)
    end
  end
end
