defmodule Test.Wialon1Test do
  use ExUnit.Case, async: false
  import Mock, warn: false

  @tcp_attrs [:binary, packet: :line, active: false, reuseaddr: true]

  setup do
    {:ok, _} = Application.ensure_all_started(:server)
    # Node.start(:"test_tcp@172.25.78.153")
    # Node.set_cookie(:"8FWrDX6nB9zCreE5")
    # Node.connect(:"global@172.25.78.153")
    port = Application.get_env(:server, :port) || 9876
    # port = 9090
    host = "127.0.0.1" |> String.to_charlist()

    # Тесты работают паралельно, поэтому к imei нужно добавлять число с номером теста
    imei = 123_456_789

    %{port: port, host: host, auth: %{imei: imei, password: "valid_password"}}
  end

  describe "wialon v1.1 // [пакет авторизации]:" do
    test "валидный imei и пароль", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      {:ok, reply} = send_and_fetch(socket, "#L##{imei}1;#{password}\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#1\r\n"
    end

    test "невалидный пароль", %{auth: %{imei: imei}} = sock_info do
      socket = connect(sock_info)
      {:ok, reply} = send_and_fetch(socket, "#L##{imei}2;invalid_passwd\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#01\r\n"
    end
  end

  describe "wialon v1.1 // [сокращенный пакет]:" do
    test "валидный пакет", sock_info do
      reply =
        do_send(
          sock_info,
          "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13\r\n"
        )

      assert reply == "#ASD#1\r\n"
    end

    test "не валидная дата/время", sock_info do
      reply = do_send(sock_info, "#SD#0;#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13\r\n")
      assert reply == "#ASD#0\r\n"
    end

    test "не валидные координаты", sock_info do
      reply =
        do_send(
          sock_info,
          "#SD##{curr_date()};#{curr_time()};55044.6025;N;03739.6834;E;10;56;100;13\r\n"
        )

      assert reply == "#ASD#10\r\n"
    end

    test "не валидная скорость", sock_info do
      reply =
        do_send(
          sock_info,
          "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;-10;56;100;13\r\n"
        )

      assert reply == "#ASD#11\r\n"
    end

    test "не валидное количество спутников", sock_info do
      reply =
        do_send(
          sock_info,
          "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;-13\r\n"
        )

      assert reply == "#ASD#12\r\n"
    end

    test "не валидная структура (добавлены лишние параметры)", sock_info do
      reply =
        do_send(
          sock_info,
          "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;758;321\r\n"
        )

      assert reply == "#ASD#-1\r\n"
    end
  end

  describe "wialon v1.1 // [расширеный пакет]:" do
    test "валидный пакет", sock_info do
      reply =
        do_send(
          sock_info,
          "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1\r\n"
        )

      assert reply == "#AD#1\r\n"
    end

    test "не валидная дата/время", sock_info do
      reply =
        do_send(
          sock_info,
          "#D#0;#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1\r\n"
        )

      assert reply == "#AD#0\r\n"
    end

    test "не валидные координаты", sock_info do
      reply =
        do_send(
          sock_info,
          "#D##{curr_date()};#{curr_time()};55044.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1\r\n"
        )

      assert reply == "#AD#10\r\n"
    end

    test "не валидная скорость", sock_info do
      reply =
        do_send(
          sock_info,
          "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;-10;56;100;13;100;NA;NA;;NA;adc:1:1\r\n"
        )

      assert reply == "#AD#11\r\n"
    end

    test "не валидное количество спутников", sock_info do
      reply =
        do_send(
          sock_info,
          "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;-13;100;NA;NA;;NA;adc:1:1\r\n"
        )

      assert reply == "#AD#12\r\n"
    end

    test "не валидная структура (добавлены лишние параметры)", sock_info do
      reply =
        do_send(
          sock_info,
          "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;758;321\r\n"
        )

      assert reply == "#AD#-1\r\n"
    end
  end

  describe "wialon v1.1 // [пакет проверки соединения]:" do
    test "_", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      body = "2.0;#{imei}#{Enum.random(1..1_000_000)};#{password}"
      {:ok, _reply} = send_and_fetch(socket, "#L##{body}\r\n")
      {:ok, reply} = send_and_fetch(socket, "#P#\r\n")
      :gen_tcp.close(socket)

      assert reply == "#AP#\r\n"
    end
  end

  defp do_send(%{auth: %{imei: imei, password: password}} = sock_info, msg) do
    socket = connect(sock_info)

    {:ok, _reply} =
      send_and_fetch(socket, "#L##{imei}#{Enum.random(1..1_000_000)};#{password}\r\n")

    {:ok, reply} = send_and_fetch(socket, msg)
    :gen_tcp.close(socket)

    reply
  end

  defp curr_date() do
    :calendar.local_time() 
    |> elem(0) 
    |> Date.from_erl!() 
    |> Date.to_string 
    |> String.replace("-", "")
  end

  defp curr_time() do
    :calendar.local_time() 
    |> elem(1) 
    |> Time.from_erl! 
    |> Time.to_string 
    |> String.replace(":", "")
  end

  defp connect(%{host: host, port: port}) do
    {:ok, socket} = :gen_tcp.connect(host, port, @tcp_attrs)
    socket
  end

  defp send_and_fetch(socket, msg, timeout \\ 1000) do
    :ok = send_msg(socket, msg)
    await_msg(socket, timeout)
  end

  defp send_msg(socket, msg), do: :gen_tcp.send(socket, msg)
  defp await_msg(socket, timeout), do: :gen_tcp.recv(socket, 0, timeout)
end
