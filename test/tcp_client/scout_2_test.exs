defmodule Test.Scout2Test do
  use ExUnit.Case, async: false
  import Mock, warn: false

  @tcp_attrs [:binary, active: false, reuseaddr: true]

  setup do
    {:ok, _} = Application.ensure_all_started(:server)
    port = Application.get_env(:server, :scout_port)
    host = "127.0.0.1" |> String.to_charlist()

    pack =
      <<186, 0, 3, 0, 62, 0, 1, 0, 44, 0, 0, 0, 6, 53, 49, 56, 48, 52, 55, 128, 130, 26, 96, 191,
        63, 218, 8, 151, 223, 242, 65, 64, 220, 111, 66, 0, 0, 0, 0, 124, 0, 20, 1, 52, 148, 114,
        0, 5, 128, 10, 0, 10, 8, 15, 0, 80, 0, 50, 0, 94, 0, 3, 0, 2, 1, 10, 62, 0, 1, 0, 44, 0,
        0, 0, 6, 53, 49, 56, 48, 52, 55, 0, 25, 179, 96, 191, 63, 218, 8, 151, 223, 242, 65, 64,
        220, 111, 66, 0, 0, 0, 0, 124, 0, 20, 1, 52, 148, 114, 0, 5, 128, 10, 0, 10, 8, 15, 0, 80,
        0, 50, 0, 94, 0, 3, 0, 2, 1, 0, 57, 0, 1, 0, 44, 0, 0, 0, 6, 53, 49, 56, 48, 52, 55, 128,
        220, 124, 98, 191, 63, 218, 8, 151, 223, 242, 65, 64, 220, 111, 66, 0, 0, 0, 0, 124, 0,
        20, 1, 52, 148, 114, 0, 5, 128, 10, 0, 10, 8, 15, 0, 80, 0, 50, 0, 94, 0, 221>>

    parsed = %{
      crc8: 221,
      header: %{frame_size: 186, packs_count: 3},
      packs: [
        %{
          course: 124,
          data:
            "%{course: 124, device_dt: ~N[2022-05-27 09:00:41], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 10, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
          device_dt: ~N[2022-05-27 09:00:41],
          device_type: 44,
          fix: 1,
          ignition_on: 1,
          lat: 59.965088,
          lon: 30.359175,
          main_power: 1,
          movement: 0,
          odometer: 7_509_044,
          pack_size: 62,
          pack_type: 1,
          satellites_count: 20,
          sensors: [
            %{
              data: "0F00500032005E00",
              data_size: 8,
              port: 10,
              sensor_num: 0,
              sensor_type: 10
            },
            %{data: 10, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}
          ],
          serial: "518047",
          serial_len: 6,
          speed: 0.0,
          status0: 1,
          status1: 32773,
          status1_valid: 1
        },
        %{
          course: 124,
          data:
            "%{course: 124, device_dt: ~N[2022-05-27 09:00:42], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
          device_dt: ~N[2022-05-27 09:00:42],
          device_type: 44,
          fix: 1,
          ignition_on: 1,
          lat: 59.965088,
          lon: 30.359175,
          main_power: 1,
          movement: 0,
          odometer: 7_509_044,
          pack_size: 62,
          pack_type: 1,
          satellites_count: 20,
          sensors: [
            %{
              data: "0F00500032005E00",
              data_size: 8,
              port: 10,
              sensor_num: 0,
              sensor_type: 10
            },
            %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}
          ],
          serial: "518047",
          serial_len: 6,
          speed: 0.0,
          status0: 1,
          status1: 32773,
          status1_valid: 1
        },
        %{
          course: 124,
          data:
            "%{course: 124, device_dt: ~N[2022-05-27 09:00:45], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 57, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
          device_dt: ~N[2022-05-27 09:00:45],
          device_type: 44,
          fix: 1,
          ignition_on: 1,
          lat: 59.965088,
          lon: 30.359175,
          main_power: 1,
          movement: 0,
          odometer: 7_509_044,
          pack_size: 57,
          pack_type: 1,
          satellites_count: 20,
          sensors: [
            %{
              data: "0F00500032005E00",
              data_size: 8,
              port: 10,
              sensor_num: 0,
              sensor_type: 10
            }
          ],
          serial: "518047",
          serial_len: 6,
          speed: 0.0,
          status0: 1,
          status1: 32773,
          status1_valid: 1
        }
      ]
    }

    # Тесты работают паралельно, поэтому к imei нужно добавлять число с номером теста
    imei = 123_456_789

    %{
      port: port,
      host: host,
      pack: pack,
      auth: %{imei: imei, password: "valid_password"},
      parsed: parsed
    }
  end

  describe "scout v2 // [пакет]:" do
    test "Парсинг пакетов", %{pack: pack, parsed: parsed} do
      line = :binary.encode_hex(pack)
      assert Scout2.Packages.DataPackage.body(line) == parsed
    end

    test "Получение пакетов", %{pack: pack} = sock_info do
      reply = do_send(sock_info, pack)
      assert reply == :binary.decode_hex("55")
    end
  end

  defp connect(%{host: host, port: port}) do
    {:ok, socket} = :gen_tcp.connect(host, port, @tcp_attrs)
    socket
  end

  defp send_and_fetch(socket, msg, timeout \\ 1000) do
    :ok = send_msg(socket, msg)
    await_msg(socket, timeout)
  end

  defp send_msg(socket, msg), do: :gen_tcp.send(socket, msg)
  defp await_msg(socket, timeout), do: :gen_tcp.recv(socket, 0, timeout)

  defp do_send(%{auth: _} = sock_info, msg) do
    socket = connect(sock_info)
    {:ok, reply} = send_and_fetch(socket, msg)
    :gen_tcp.close(socket)
    reply
  end
end
