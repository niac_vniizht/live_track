defmodule Test.Scout2BufferTest do
  use ExUnit.Case, async: false
  import Mock, warn: false

  setup do
    pack_flow_1 =
      <<67, 0, 1, 0, 62, 0, 1, 0, 44, 0, 0, 0, 6, 53, 49, 56, 48, 52, 55, 128, 180, 6, 9, 163, 63,
        218, 8, 174, 223, 242, 65, 109, 220, 111, 66, 0, 0, 0, 0, 22, 0, 16, 1, 34, 148, 114, 0,
        5, 128, 10, 0, 10, 8, 29, 0, 100, 0, 60, 0, 116, 0, 3, 0, 2, 1, 0, 74>>

    pack_flow_2 =
      <<186, 0, 3, 0, 62, 0, 1, 0, 44, 0, 0, 0, 6, 53, 49, 56, 48, 52, 55, 128, 130, 26, 96, 191,
        63, 218, 8, 151, 223, 242, 65, 64, 220, 111, 66, 0, 0, 0, 0, 124, 0, 20, 1, 52, 148, 114,
        0, 5, 128, 10, 0, 10, 8, 15, 0, 80, 0, 50, 0, 94, 0, 3, 0, 2, 1, 10, 62, 0, 1, 0, 44, 0,
        0, 0, 6, 53, 49, 56, 48, 52, 55, 0, 25, 179, 96, 191, 63, 218, 8, 151, 223, 242, 65, 64,
        220, 111, 66, 0, 0, 0, 0, 124, 0, 20, 1, 52, 148, 114, 0, 5, 128, 10, 0, 10, 8, 15, 0, 80,
        0, 50, 0, 94, 0, 3, 0, 2, 1, 0, 57, 0, 1, 0, 44, 0, 0, 0, 6, 53, 49, 56, 48, 52, 55, 128,
        220, 124, 98, 191, 63, 218, 8, 151, 223, 242, 65, 64, 220, 111, 66, 0, 0, 0, 0, 124, 0,
        20, 1, 52, 148, 114, 0, 5, 128, 10, 0, 10, 8, 15, 0, 80, 0, 50, 0, 94, 0, 221>>

    auth = %{body: %{imei: "518047"}}

    messages_1 = [
      %{
        course: 22,
        data:
          "%{course: 22, data: \"%{course: 22, device_dt: ~N[2022-05-27 05:37:49], device_type: 44, fix: 1, ignition_on: 1, lat: 59.96526, lon: 30.359219, main_power: 1, movement: 0, odometer: 7509026, pack_size: 62, pack_type: 1, satellites_count: 16, sensors: [%{data: \\\"1D0064003C007400\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 05:37:49], device_type: 44, fix: 1, ignition_on: 1, lat: 59.96526, lon: 30.359219, main_power: 1, movement: 0, odometer: 7509026, pack_size: 62, pack_type: 1, satellites_count: 16, sensors: [%{data: \"1D0064003C007400\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
        device_type: 44,
        imei: "518047",
        lat: 59.96526,
        lon: 30.359219,
        odometer: 7_509_026,
        pack_dt: ~N[2022-05-27 05:37:49],
        server_dt: ~N[2022-06-01 07:01:30.738744],
        speed: 0.0
      }
    ]

    messages_2 = [
      %{
        course: 124,
        data:
          "%{course: 124, data: \"%{course: 124, device_dt: ~N[2022-05-27 09:00:41], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \\\"0F00500032005E00\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 10, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 09:00:41], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 10, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
        device_type: 44,
        imei: "518047",
        lat: 59.965088,
        lon: 30.359175,
        odometer: 7_509_044,
        pack_dt: ~N[2022-05-27 09:00:41],
        server_dt: ~N[2022-06-01 07:25:23.424152],
        speed: 0.0
      },
      %{
        course: 124,
        data:
          "%{course: 124, data: \"%{course: 124, device_dt: ~N[2022-05-27 09:00:42], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \\\"0F00500032005E00\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 09:00:42], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
        device_type: 44,
        imei: "518047",
        lat: 59.965088,
        lon: 30.359175,
        odometer: 7_509_044,
        pack_dt: ~N[2022-05-27 09:00:42],
        server_dt: ~N[2022-06-01 07:25:23.424240],
        speed: 0.0
      },
      %{
        course: 124,
        data:
          "%{course: 124, data: \"%{course: 124, device_dt: ~N[2022-05-27 09:00:45], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 57, pack_type: 1, satellites_count: 20, sensors: [%{data: \\\"0F00500032005E00\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 09:00:45], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 57, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}",
        device_type: 44,
        imei: "518047",
        lat: 59.965088,
        lon: 30.359175,
        odometer: 7_509_044,
        pack_dt: ~N[2022-05-27 09:00:45],
        server_dt: ~N[2022-06-01 07:25:23.424346],
        speed: 0.0
      }
    ]

    log_data_1 = %{
      log_type: "info",
      message:
        "%{crc8: 74, header: %{frame_size: 67, packs_count: 1}, packs: [%{course: 22, data: \"%{course: 22, device_dt: ~N[2022-05-27 05:37:49], device_type: 44, fix: 1, ignition_on: 1, lat: 59.96526, lon: 30.359219, main_power: 1, movement: 0, odometer: 7509026, pack_size: 62, pack_type: 1, satellites_count: 16, sensors: [%{data: \\\"1D0064003C007400\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 05:37:49], device_type: 44, fix: 1, ignition_on: 1, lat: 59.96526, lon: 30.359219, main_power: 1, movement: 0, odometer: 7509026, pack_size: 62, pack_type: 1, satellites_count: 16, sensors: [%{data: \"1D0064003C007400\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}]}",
      state: "server",
      version: 2
    }

    log_data_2 = %{
      log_type: "info",
      message:
        "%{crc8: 221, header: %{frame_size: 186, packs_count: 3}, packs: [%{course: 124, data: \"%{course: 124, device_dt: ~N[2022-05-27 09:00:41], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \\\"0F00500032005E00\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 10, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 09:00:41], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 10, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}, %{course: 124, data: \"%{course: 124, device_dt: ~N[2022-05-27 09:00:42], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \\\"0F00500032005E00\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 09:00:42], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 62, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}, %{data: 0, data_size: 1, port: 3, sensor_num: 0, sensor_type: 2}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}, %{course: 124, data: \"%{course: 124, device_dt: ~N[2022-05-27 09:00:45], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 57, pack_type: 1, satellites_count: 20, sensors: [%{data: \\\"0F00500032005E00\\\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}], serial: \\\"518047\\\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}\", device_dt: ~N[2022-05-27 09:00:45], device_type: 44, fix: 1, ignition_on: 1, lat: 59.965088, lon: 30.359175, main_power: 1, movement: 0, odometer: 7509044, pack_size: 57, pack_type: 1, satellites_count: 20, sensors: [%{data: \"0F00500032005E00\", data_size: 8, port: 10, sensor_num: 0, sensor_type: 10}], serial: \"518047\", serial_len: 6, speed: 0.0, status0: 1, status1: 32773, status1_valid: 1}]}",
      state: "server",
      version: 2
    }

    %{
      auth: auth,
      pack_flow_1: pack_flow_1,
      pack_flow_2: pack_flow_2,
      messages_1: messages_1,
      log_data_1: log_data_1,
      messages_2: messages_2,
      log_data_2: log_data_2,
      respond: :binary.decode_hex("55")
    }
  end

  # describe "scout2 // проверка парсинга пакетов" do
  #   test "парсинг потока пакетов, количество пакетов = 1", ~M{
  #     pack_flow_1 ,messages_1, log_data_1, auth, respond
  #   } do
  #     assert {:ok,
  #             %LiveTrack.Structures.Scout2Buffer{
  #               auth: ^auth,
  #               finish: false,
  #               log: ^log_data_1,
  #               messages: res_messages_1,
  #               opts: _,
  #               respond: ^respond,
  #               type: _,
  #               version: _
  #             }} = LiveTrack.Structures.Scout2Buffer.new(pack_flow_1)

  #     assert Enum.map(messages_1, &Map.put(&1, :server_dt, nil)) ==
  #              Enum.map(res_messages_1, &Map.put(&1, :server_dt, nil))
  #   end

  #   test "парсинг потока пакетов, количество пакетов = 3", ~M{
  #     pack_flow_2, messages_2, log_data_2, auth, respond
  #   } do
  #     assert {:ok,
  #             %LiveTrack.Structures.Scout2Buffer{
  #               auth: ^auth,
  #               finish: false,
  #               log: ^log_data_2,
  #               messages: res_messages_2,
  #               opts: _,
  #               respond: ^respond,
  #               type: _,
  #               version: 2
  #             }} = LiveTrack.Structures.Scout2Buffer.new(pack_flow_2)

  #     assert Enum.map(messages_2, &Map.put(&1, :server_dt, nil)) ==
  #              Enum.map(res_messages_2, &Map.put(&1, :server_dt, nil))
  #   end
  end
end
