defmodule Test.NiacBufferTest do
  use ExUnit.Case, async: false
  import Mock, warn: false

  # 868136031152476
  # 333838383137510E00280039
  # 1,12415200,61120,63538270,34278805,3534,8960,2048,66,16,1941,0,29646,1301,3003,0,3235,4,2,4095,4095,4095,4095,4095,4095,0,127,0,0,0,0

  setup do
    imei = 999_999_999
    valid_auth_pack = "2,#{imei}1,key\r\n"
    invalid_auth_pack_1 = "1,#{imei}1,key\r\n"
    invalid_auth_pack_2 = "2,#{imei}1,key,p1,p2\r\n"

    log_auth = %{
      log_type: "info",
      message: "%{auth_params: [\"2\", \"9999999991\", \"key\"], imei: \"9999999991\"}",
      state: "server"
    }

    niac_login = %Niac.Packages.Login{
      body: %{auth_params: ["2", "9999999991", "key"], imei: "9999999991"},
      log: log_auth
    }

    data_pack = "1,2,3,4,5,6\r\n"

    messages_1 = ["1,2,3,4,5,6"]

    log_data = %{
      log_type: "info",
      message: inspect(%{message: "1,2,3,4,5,6"}),
      state: "server"
    }

    part_data_pack_1 = "1,2,3,4,5,"
    part_data_pack_2 = "6\r\n"

    log_part = %{
      log_type: "info",
      message: "%{message_part: \"1,2,3,4,5,\"}",
      state: "server"
    }

    %{
      imei: imei,
      valid_auth_pack: valid_auth_pack,
      invalid_auth_pack_1: invalid_auth_pack_1,
      invalid_auth_pack_2: invalid_auth_pack_2,
      log_auth: log_auth,
      niac_login: niac_login,
      data_pack: data_pack,
      messages_1: messages_1,
      log_data: log_data,
      part_data_pack_1: part_data_pack_1,
      part_data_pack_2: part_data_pack_2,
      log_part: log_part
    }
  end

  describe "niac // проверка авторизации" do
    test "валидная авторизация", %{valid_auth_pack: valid_auth_pack} do
      assert LiveTrack.Structures.NiacBuffer.is_valid_login(valid_auth_pack) == true
    end

    test "невалидная авторизация", %{
      invalid_auth_pack_1: invalid_auth_pack_1,
      invalid_auth_pack_2: invalid_auth_pack_2
    } do
      assert LiveTrack.Structures.NiacBuffer.is_valid_login(invalid_auth_pack_1) == false
      assert LiveTrack.Structures.NiacBuffer.is_valid_login(invalid_auth_pack_2) == false
    end
  end

  describe "niac // проверка парсинга пакетов" do
    test "парсинг авторизации", %{
      valid_auth_pack: valid_auth_pack,
      log_auth: log_auth,
      niac_login: niac_login
    } do
      assert {
               :ok,
               %LiveTrack.Structures.NiacBuffer{
                 auth: ^niac_login,
                 finish: false,
                 logs: [^log_auth],
                 message_part: "",
                 messages: []
               }
             } = LiveTrack.Structures.NiacBuffer.new(valid_auth_pack)
    end

    test "парсинг пакета", %{
      valid_auth_pack: valid_auth_pack,
      niac_login: niac_login,
      data_pack: data_pack,
      messages_1: messages_1,
      log_data: log_data
    } do
      {:ok, buffer} = LiveTrack.Structures.NiacBuffer.new(valid_auth_pack)

      assert %LiveTrack.Structures.NiacBuffer{
               auth: ^niac_login,
               finish: false,
               logs: [_, ^log_data],
               message_part: "",
               messages: ^messages_1
             } = LiveTrack.Structures.NiacBuffer.add(buffer, data_pack)
    end

    test "парсинг незавершённого пакета", %{
      valid_auth_pack: valid_auth_pack,
      messages_1: messages_1,
      log_data: log_data,
      part_data_pack_1: part_data_pack_1,
      part_data_pack_2: part_data_pack_2,
      log_part: log_part
    } do
      {:ok, buffer} = LiveTrack.Structures.NiacBuffer.new(valid_auth_pack)

      buffer = LiveTrack.Structures.NiacBuffer.add(buffer, part_data_pack_1)

      assert %LiveTrack.Structures.NiacBuffer{
               finish: false,
               logs: [_, ^log_part],
               message_part: ^part_data_pack_1,
               messages: []
             } = buffer

      assert %LiveTrack.Structures.NiacBuffer{
               finish: false,
               logs: [_, ^log_part, ^log_data],
               message_part: "",
               messages: ^messages_1
             } = LiveTrack.Structures.NiacBuffer.add(buffer, part_data_pack_2)
    end

    test "парсинг конечного пакета", %{
      valid_auth_pack: valid_auth_pack
    } do
      {:ok, buffer} = LiveTrack.Structures.NiacBuffer.new(valid_auth_pack)

      assert %LiveTrack.Structures.NiacBuffer{
               finish: true
             } = LiveTrack.Structures.NiacBuffer.add(buffer, "0\r\n")
    end
  end
end
